package com.propertypassport.web.hcs;

import java.io.FileReader;
import java.util.Objects;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Statics {
    private static AppData appData;
    private static final Logger logger = LoggerFactory.getLogger(Statics.class);
    
    public static AppData getAppData() throws Exception {
    
    	
    	Properties p = new Properties();
    	FileReader reader=new FileReader("src/main/resources/application.properties");
		if(Objects.nonNull(reader)) {
			try {
				p.load(reader);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
        String network = p.getProperty("network");
        if (Objects.isNull(appData)) {
        	if(network.equalsIgnoreCase("prod")) {
            appData = new AppData("./config-prod/config.yaml","./config-prod/.env","./config-prod/docker-compose.yml");
        	}
        	else if(network.equalsIgnoreCase("stage")) {
        		 appData = new AppData("./config-stage/config.yaml","./config-stage/.env","./config-stage/docker-compose.yml");
        	}
        	else {
        		 appData = new AppData("./config-dev/config.yaml","./config-dev/.env","./config-dev/docker-compose.yml");
        	}
        }
        
        return appData;
    }
}
