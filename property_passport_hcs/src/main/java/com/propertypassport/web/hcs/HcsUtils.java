package com.propertypassport.web.hcs;

import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Service;

import com.hedera.hashgraph.sdk.AccountId;
import com.hedera.hashgraph.sdk.Client;
import com.hedera.hashgraph.sdk.PrivateKey;


@Service
public class HcsUtils {

	public static  Client createHederaClient() throws InterruptedException {
		var networkName = "testnet";
		var mirrorNetwork = "hcs.testnet.mirrornode.hedera.com:5600";
		System.out.println("mirrorNetwork....." + mirrorNetwork);
		Client client;

		// noinspection EnhancedSwitchMigration
		switch (networkName) {
		case "mainnet":
			client = Client.forMainnet();

			System.out.println("Create Hedera client for Mainnet");

			System.out.println("Using {} to connect to the hedera mirror network" + mirrorNetwork);
			client.setMirrorNetwork(List.of(mirrorNetwork));
			break;
		case "testnet":
			System.out.println("Create Hedera client for Testnet");

			client = Client.forTestnet();
			client.setMirrorNetwork(List.of(mirrorNetwork));
			System.out.println("Using {} to connect to the hedera mirror network" + mirrorNetwork);
			System.out.println("The hedera mirror network" + client.getMirrorNetwork());
			System.out.println("The network" + client.getNetwork());

			break;
		default:
			throw new IllegalStateException("unknown hedera network name: " + networkName);
		}

		//var operatorId = "0.0.4389";
		final AccountId ADMIN_OPERATOR_ID = AccountId.fromString(Objects.requireNonNull("0.0.4389"));
		 final PrivateKey ADMIN_OPERATOR_KEY = PrivateKey.fromString(Objects.requireNonNull(
					"302e020100300506032b657004220420b56be9ea5c14be403fa44280fe85457ccbda9388e161b10ce1bfa072d1fcd2ab"));
		 
		System.out.println("operatorId {}" + ADMIN_OPERATOR_ID);
		//var operatorKey = "302e020100300506032b657004220420b56be9ea5c14be403fa44280fe85457ccbda9388e161b10ce1bfa072d1fcd2ab";
		System.out.println("operatorKey {}" + ADMIN_OPERATOR_KEY);

		if (ADMIN_OPERATOR_ID != null && ADMIN_OPERATOR_KEY != null) {
			client.setOperator(ADMIN_OPERATOR_ID, ADMIN_OPERATOR_KEY);
		}
		// logger.info("Topic ID ", client);

		System.out.println("The OperatorPublicKey {}" + client.getOperatorPublicKey());

		System.out.println("The OperatorAccountId {}" + client.getOperatorAccountId());
		System.out.println("HederaClinet....." + client);
		return client;
	}
}
