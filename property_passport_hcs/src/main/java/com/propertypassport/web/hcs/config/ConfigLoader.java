package com.propertypassport.web.hcs.config;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.propertypassport.web.hcs.CreateTopic;

public class ConfigLoader {
    private static ConfigLoader configLoader;
    private static final Logger logger = LoggerFactory.getLogger(CreateTopic.class);
    
    public static void loadConfiguration(String path) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            ConfigLoader.configLoader = mapper.readValue(new File(path), ConfigLoader.class);
        } catch (Exception e) {
        	logger.info("Exception: " + e.getMessage());
        }
    }

    public static ConfigLoader getConfigLoader() {
        if(configLoader == null) {
            loadConfiguration("exo-config.json");
        }
        return configLoader;
    }
}