package com.propertypassport.web.hcs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.propertypassport.web.dataaccess.common.request.PropertyOwnerInfoDetails;
import com.propertypassport.web.hcs.service.HcsTemplate;

@CrossOrigin
@RestController
@RequestMapping(value = "/api")
public class HcsController {

	@Autowired
	HcsTemplate hcsTemplate;

	@PostMapping("/publishmirronode")
	@PreAuthorize("hasRole('USER') or hasRole('MANAGER') or hasRole('ADMIN')")
	public ResponseEntity<?> publish(@RequestBody String inputByte) {
		return hcsTemplate.publisToMirrorNode(inputByte);
	}

	@PostMapping("/publishmirronode/user")
	public ResponseEntity<?> publishUser(@RequestBody String inputByte) {
		return hcsTemplate.publisToMirrorNode(inputByte);
	}

	@PostMapping("/publishmirronode/propertyinfo")
	public ResponseEntity<?> publishProertyInfo(@RequestBody String topicId,@RequestBody String operatorId,
			@RequestBody PropertyOwnerInfoDetails propertyOwnerInfoDetails) {
		return hcsTemplate.publishProertyInfo(topicId,operatorId, propertyOwnerInfoDetails);
	}
}
