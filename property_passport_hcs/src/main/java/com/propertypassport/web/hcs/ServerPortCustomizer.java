package com.propertypassport.web.hcs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*-
 * ‌
 * hcs-sxc-java
 * ​
 * Copyright (C) 2019 - 2020 Hedera Hashgraph, LLC
 * ​
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ‍
 */

import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Component
public class ServerPortCustomizer 
  implements WebServerFactoryCustomizer<ConfigurableWebServerFactory> {
	
	private static final Logger logger = LoggerFactory.getLogger(ServerPortCustomizer.class);
	
    @Override
    public void customize(ConfigurableWebServerFactory factory) {
        try {
           // factory.setPort(Statics.getAppData().getWebPort());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }
}
