package com.propertypassport.web.hcs;

/*-
 * ‌
 * hcs-sxc-java
 * ​
 * Copyright (C) 2019 - 2020 Hedera Hashgraph, LLC
 * ​
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ‍
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hedera.hashgraph.sdk.AccountId;
import com.hedera.hashgraph.sdk.PrivateKey;
import com.hedera.hcs.sxc.HCSCore;
import com.hedera.hcs.sxc.utils.StringUtils;
import com.propertypassport.web.hcs.appconfig.AppClient;
import com.propertypassport.web.hcs.dockercomposereader.DockerCompose;
import com.propertypassport.web.hcs.dockercomposereader.DockerComposeReader;
import com.propertypassport.web.hcs.dockercomposereader.DockerService;

import io.github.cdimascio.dotenv.Dotenv;

public final class AppData {

    private HCSCore hcsCore;
    private int topicIndex = 0; // refers to the first topic ID in the config.yaml
    private String publicKey = "";
    private PrivateKey operatorKey;
    private AccountId operatorId = new AccountId(0,0,0);
    private byte[] messageEncryptionKey = new byte[0];
    private String userName = "";
    private String appId = "";
    List<AppClient> appClients = new ArrayList<>();
    private int webPort = 8080;
    private Dotenv dotEnv;
    
	private static final Logger logger = LoggerFactory.getLogger(AppData.class);
	
    private String getEnvValue(String varName) throws Exception {
        String value = "";
        System.out.println("Looking for " + varName + " in environment variables");
        if (System.getProperty(varName) != null) {
            value = System.getProperty(varName);
            logger.info(varName + " found in command line parameters");
        } else if ((this.dotEnv == null) || (this.dotEnv.get(varName) == null) || (this.dotEnv.get(varName).isEmpty())) {
            logger.info(varName + " environment variable is not set");
            logger.info(varName + " environment variable not found in ./config/.env");
            logger.info(varName + " environment variable not found in command line parameters");
            System.exit(0);
        } else {
            value = this.dotEnv.get(varName);
            logger.info(varName + " found in environment variables");
        }
        return value;
    }

    private String getOptionalEnvValue(String varName) throws Exception {
        String value = "";
        logger.info("Looking for " + varName + " in environment variables");
        if (System.getProperty(varName) != null) {
            value = System.getProperty(varName);
            logger.info(varName + " found in command line parameters");
        } else if ((this.dotEnv == null) || (this.dotEnv.get(varName) == null)) {
            value = "";
        } else {
            value = this.dotEnv.get(varName);
            logger.info(varName + " found in environment variables");
        }
        return value;
    }

    private void loadEnv(String envPath) throws Exception {
        this.dotEnv = Dotenv.configure().filename(envPath).ignoreIfMissing().load();
        this.appId = getOptionalEnvValue("APP_ID");
        String encryptionKey = getOptionalEnvValue("ENCRYPTION_KEY");
        if (encryptionKey.isEmpty()) {
            this.messageEncryptionKey = new byte[0];
        } else {
            this.messageEncryptionKey = StringUtils.hexStringToByteArray(encryptionKey);
        }
    }

    public AppData() throws Exception {
        loadEnv("./config/.env");
        init("./config/config.yaml", "./config/docker-compose.yml", "./config/.env");
    }
    public AppData(String configFilePath, String environmentFilePath, String dockerFilePath) throws Exception {
        loadEnv(environmentFilePath);
        init(configFilePath, dockerFilePath, environmentFilePath);
    }
    private void init(String configFilePath, String dockerFilePath, String environmentFilePath) throws Exception {
        DockerCompose dockerCompose = DockerComposeReader.parse(dockerFilePath);
  
        if ( System.getProperty("server.port") != null ) { 
            this.webPort = Integer.parseInt(System.getProperty("server.port"));
            System.out.println("PORT=" + this.webPort + " found in command line parameter server.port");
        } else {
            this.webPort = dockerCompose.getPortForId(this.appId);
            logger.info("PORT=" + this.webPort + " found in docker compose");
        }

        this.publicKey = dockerCompose.getPublicKeyForId(this.appId);
        this.userName = dockerCompose.getNameForId(this.appId);
        this.topicIndex = 0;
        
        if (publicKey.equalsIgnoreCase("not found") || publicKey.equalsIgnoreCase("not found")){
            System.out.println("The chosen APP_ID must be present in the docker-compose config file. Exiting ...");
            System.exit(0);
        }

        for (Map.Entry<String, DockerService> service : dockerCompose.getServices().entrySet()) {
            DockerService dockerService = service.getValue();
            if (dockerService.getEnvironment() != null) {
                if (dockerService.getEnvironment().containsKey("APP_ID")) {
                    AppClient appClient = new AppClient();
                    appClient.setClientKey(dockerService.getEnvironment().get("PUBKEY"));
                    appClient.setClientName(dockerService.getContainerName());
                    appClient.setPaymentAccountDetails(dockerService.getEnvironment().get("PAYMENT_ACCOUNT_DETAILS"));
                    appClient.setRoles(dockerService.getEnvironment().get("ROLES"));
                    appClient.setColor(dockerService.getEnvironment().get("COLOR"));
                    appClient.setAppId(dockerService.getEnvironment().get("APP_ID"));
                    appClient.setWebPort(Integer.parseInt(Utils.getPort(dockerService)));

                    this.appClients.add(appClient);
                }
            }
        }
        this.hcsCore =  HCSCore.INSTANCE.singletonInstance(this.appId, configFilePath, environmentFilePath)
                .withMessageEncryptionKey(this.messageEncryptionKey);
    }

    public HCSCore getHCSCore() {
        return this.hcsCore;
    }

    public String getAppId() {
        return this.appId;
    }

    public String getPublicKey() {
        return this.publicKey;
    }

    public String getUserName() {
        return this.userName;
    }

    public int getTopicIndex() {
        return this.topicIndex;
    }

    public List<AppClient> getAppClients() {
        return this.appClients;
    }
    
    public int getWebPort() {
        return this.webPort;
    }
    
    public PrivateKey getOperatorKey() {
        return this.operatorKey;
    }
    public AccountId getOperatorId() {
        return this.operatorId;
    }

	public void setTopicIndex(int topicIndex) {
		this.topicIndex = topicIndex;
	}

	public void setOperatorKey(PrivateKey operatorKey) {
		this.operatorKey = operatorKey;
	}

	public void setOperatorId(AccountId operatorId) {
		this.operatorId = operatorId;
	}
    
}