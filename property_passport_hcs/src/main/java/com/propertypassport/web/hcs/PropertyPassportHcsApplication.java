package com.propertypassport.web.hcs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication(scanBasePackages = {"com.propertypassport.web","com.propertypassport.web.*"} )
@PropertySources({ @PropertySource("classpath:env.properties"),@PropertySource("classpath:message.properties") })
@Configuration
public class PropertyPassportHcsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PropertyPassportHcsApplication.class, args);
	}

}
