package com.propertypassport.web.hcs.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CrunchifyGetPropertyValues {
	String result = "";
	InputStream inputStream;
	private static final Logger logger = LoggerFactory.getLogger(CrunchifyGetPropertyValues.class);
	 
	public String getPropValues(String name) throws IOException {

		try {
			Properties prop = new Properties();
			String propFileName = "config.properties";

			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}

			result = prop.getProperty(name);
		} catch (Exception e) {
			logger.info("Exception: " + e.getMessage());
		} finally {
			inputStream.close();
		}
		return result;
	}
}
