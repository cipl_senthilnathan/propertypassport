package com.propertypassport.web.hcs;

/*-
 * ‌
 * hcs-sxc-java
 * ​
 * Copyright (C) 2019 - 2020 Hedera Hashgraph, LLC
 * ​
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ‍
 */

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Random;

import com.hedera.hashgraph.sdk.TransactionId;
import com.hedera.hcs.sxc.proto.AccountID;
import com.hedera.hcs.sxc.proto.ApplicationMessageID;
import com.hedera.hcs.sxc.proto.Timestamp;
import com.propertypassport.web.hcs.dockercomposereader.DockerService;

public final class Utils {
    private static Random random = new Random();

    public static String timestampToDate(long seconds, int nanos) {
        LocalDateTime dateTime = LocalDateTime.ofEpochSecond(seconds, nanos, ZoneOffset.UTC);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM", Locale.getDefault());
        String formattedDate = dateTime.format(formatter);
        return formattedDate;
    }
    
    public static String timestampToTime(long seconds, int nanos) {
        LocalDateTime dateTime = LocalDateTime.ofEpochSecond(seconds, nanos, ZoneOffset.UTC);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("H:mm", Locale.getDefault());
        String formattedDate = dateTime.format(formatter);
        return formattedDate;
    }
    
    public static String applicationMessageIdToString(ApplicationMessageID transactionId) {
        String txId = "0.0." + transactionId.getAccountID().getAccountNum()
                + "-" + transactionId.getValidStart().getSeconds()
                + "-" + transactionId.getValidStart().getNanos();
        return txId;
    }
    
//    public static String transactionIdToString(TransactionId transactionId) {
//        String txId = "0.0." + transactionId.accountId.account
//                + "-" + transactionId.validStart.getEpochSecond()
//                + "-" + transactionId.validStart.getNano();
//        return txId;
//    }
    
    

    public static ApplicationMessageID applicationMessageIdFromString(String appMessageId) {
        String[] messageIdParts = appMessageId.split("-");
        String[] account = messageIdParts[0].split("\\.");
        
        AccountID accountId = AccountID.newBuilder()
                .setShardNum(Long.parseLong(account[0]))
                .setRealmNum(Long.parseLong(account[1]))
                .setAccountNum(Long.parseLong(account[2]))
                .build();
        
        Timestamp timestamp = Timestamp.newBuilder()
                .setSeconds(Long.parseLong(messageIdParts[1]))
                .setNanos(Integer.parseInt(messageIdParts[2]))
                .build();
        
        ApplicationMessageID applicationMessageId = ApplicationMessageID.newBuilder()
                .setAccountID(accountId)
                .setValidStart(timestamp)
                .build();
        
        return applicationMessageId;
    }
    
    public static String getThreadId() {
        Instant now = Instant.now();
        long nano = now.getNano();
        
        long remainder = nano - (nano / 1000 * 1000); // check nanos end with 000.
        if (remainder == 0) {
            int rndNano = random.nextInt(1000);
            nano = nano + rndNano;
        }
     
        return now.getEpochSecond() + "-" + nano;
    }
    
    public static String getPort(DockerService dockerService) {
        String[] ports = dockerService.getPorts().get(0).split(":");
        return ports[0];
    }
}
