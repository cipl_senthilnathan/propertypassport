package com.propertypassport.web.hcs.service;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.hedera.hashgraph.sdk.AccountId;
import com.hedera.hashgraph.sdk.Client;
import com.hedera.hashgraph.sdk.TopicId;
import com.hedera.hashgraph.sdk.TopicMessageQuery;
import com.hedera.hashgraph.sdk.TopicMessageSubmitTransaction;
import com.hedera.hashgraph.sdk.TransactionId;
import com.hedera.hashgraph.sdk.TransactionReceipt;
import com.hedera.hcs.sxc.consensus.OutboundHCSMessage;
import com.propertypassport.web.dataaccess.common.request.PropertyOwnerInfoDetails;
import com.propertypassport.web.dataaccess.mongo.model.ServiceType;
import com.propertypassport.web.dataaccess.payload.response.MessageResponse;
import com.propertypassport.web.dataaccess.services.MongoDBLoggerService;
import com.propertypassport.web.hcs.AppData;
import com.propertypassport.web.hcs.HcsUtils;
import com.propertypassport.web.hcs.Statics;

@Service
public class HcsTemplate {
	@Autowired
	Environment env;

	@Autowired
	MongoDBLoggerService mongoDBLoggerService;

	public ResponseEntity<?> publisToMirrorNode(String inputByte) {

		try {
			inputByte = "serverName:" + env.getProperty("serverName") + inputByte;
			AppData appData = Statics.getAppData();
			new OutboundHCSMessage(appData.getHCSCore()).overrideMessageSignature(false)
					.sendMessage(appData.getTopicIndex(), inputByte.getBytes());
		} catch (Exception e) {
			mongoDBLoggerService.createLogger(e.getMessage(), ServiceType.HCS);
			return ResponseEntity.ok(new MessageResponse(HttpStatus.BAD_REQUEST.value(), e.getMessage(), ""));
		}
		return ResponseEntity
				.ok(new MessageResponse(HttpStatus.OK.value(), env.getProperty("publish.message.success"), ""));
	}

	public ResponseEntity<?> publishProertyInfo(String topicId,String operatorId,
			@RequestBody PropertyOwnerInfoDetails propertyOwnerInfoDetails) {
		TransactionReceipt transactionReceipt=null;
		try {
			Client hederaClinet= HcsUtils.createHederaClient();
//			var operatorId = Objects.requireNonNull(
//					hederaClinet.getOperatorAccountId()
//		        );
			AccountId accountId=AccountId.fromString(operatorId);
			 var transactionId = TransactionId.generate(accountId);
			 
			 // Create topic
//			 if(Objects.isNull(topicId)) {
//				  topicId= generateTopicId(hederaClinet);
//			 }
			System.out.println("Topic ID......"+topicId);
			
			TopicId topicIdToSubscribe=TopicId.fromString(topicId);
			subscribeToTopic(topicIdToSubscribe,hederaClinet);
			
			//Subscribe to a topic
			 transactionReceipt = new TopicMessageSubmitTransaction()
	        .setMaxChunks(1)
	        .setTopicId(topicIdToSubscribe)
	        .setTransactionId(transactionId)
	        .setMessage(propertyOwnerInfoDetails.toString())
	        .execute(hederaClinet, Duration.ofMinutes(5))
	        .getReceipt(hederaClinet, Duration.ofMinutes(5));
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return ResponseEntity
				.ok(new MessageResponse(HttpStatus.OK.value(), env.getProperty("publish.message.success"), transactionReceipt));
	}
	
	/*
	 * Subscribe to a topic
	 */
	public void subscribeToTopic(TopicId topicId,Client client) {
		try {
			new TopicMessageQuery()
		    .setTopicId(topicId)
		    .subscribe(client, resp -> {
		            String messageAsString = new String(resp.contents, StandardCharsets.UTF_8);

		            System.out.println(resp.consensusTimestamp + " received topic message: " + messageAsString);
		    });
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}

}
