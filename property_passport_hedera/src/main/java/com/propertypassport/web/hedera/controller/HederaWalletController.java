package com.propertypassport.web.hedera.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.propertypassport.web.hedera.service.HederaWalletService;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/hedera")
public class HederaWalletController {

	@Autowired
	HederaWalletService hederaWalletService;
	
	@PreAuthorize("hasRole('USER') or hasRole('MANAGER') or hasRole('ADMIN')")
	@RequestMapping(value = "/createwallet", method = RequestMethod.POST)
	public ResponseEntity<?> createWallet(  ) throws Exception {
		return hederaWalletService.createWallet();
	}
}
