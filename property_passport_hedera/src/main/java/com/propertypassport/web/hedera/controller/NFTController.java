package com.propertypassport.web.hedera.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.propertypassport.web.dataaccess.payload.request.FileCreationDto;
import com.propertypassport.web.hedera.payload.request.NFTTokenDto;
import com.propertypassport.web.hedera.service.NFTTokenService;
import org.springframework.security.access.prepost.PreAuthorize;


@CrossOrigin
@RestController
@RequestMapping(value = "/api/hedera")
public class NFTController {

	@Autowired
	NFTTokenService nftTokenService;
	
	
	@PreAuthorize("hasRole('USER')")
	@RequestMapping(value = "/createNftToken", method = RequestMethod.POST)
	public ResponseEntity<?> createNFTToken(@RequestBody NFTTokenDto nftTokenDto) throws Exception {
		return nftTokenService.createNFTToken(nftTokenDto);
	}
	
	@PreAuthorize("hasRole('USER')")
	@RequestMapping(value = "/createFile", method = RequestMethod.POST)
	public ResponseEntity<?> createFile(@RequestBody FileCreationDto fileCreationDto) throws InterruptedException{
		return nftTokenService.createFile(fileCreationDto);
	}
}
