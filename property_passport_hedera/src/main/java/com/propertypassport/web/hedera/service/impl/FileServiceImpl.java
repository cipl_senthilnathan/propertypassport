package com.propertypassport.web.hedera.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hedera.hashgraph.sdk.AccountId;
import com.hedera.hashgraph.sdk.Client;
import com.hedera.hashgraph.sdk.FileCreateTransaction;
import com.hedera.hashgraph.sdk.PrivateKey;
import com.hedera.hashgraph.sdk.TransactionReceipt;
import com.propertypassport.web.hedera.utils.HCSTransactionService;

@Service
public class FileServiceImpl {

	@Autowired
	HCSTransactionService hcsTransactionService;
	
	public static void main(String[] args) throws InterruptedException {
		FileServiceImpl fileServiceImpl = new FileServiceImpl();
		// TODO Auto-generated method stub
		File file = new File("D:/Document/NFT_Documnet/tokeninfo.txt");
	       
		byte[] fileContent = readContentIntoByteArray(file);
		Client hederaClient= fileServiceImpl.createHederaClient();
		TransactionReceipt transactionReceipt = fileServiceImpl.fileCreate(file, fileContent,hederaClient);
		System.out.println("transactionReceipt....."+transactionReceipt);
	}

	public  TransactionReceipt fileCreate(File fileData,byte[] fileContent,Client hederaClient) throws InterruptedException {
		   // Publish file to Hedera File Service storage
	    var privateKey = PrivateKey.fromString("302e020100300506032b657004220420b56be9ea5c14be403fa44280fe85457ccbda9388e161b10ce1bfa072d1fcd2ab");
	  //  var client = Client.forTestnet();
	    var fileId = "";
	    var fileChunk = 4000;
	    var largeFile = fileData.getTotalSpace() > fileChunk;
	    var startIndex = 0;
	    try {
	      var keys  = privateKey;
	      var fileCreateTransaction = new FileCreateTransaction();

	      if (largeFile) {
	        // if we have a large file (> 4000 bytes), create the file with keys
	        // then run file append
	        // then remove keys
	        fileCreateTransaction.setContents(fileContent);
	        fileCreateTransaction.setKeys(keys);
	      } else {
	        fileCreateTransaction.setContents(fileContent);
	      }

	      var response = fileCreateTransaction.execute(hederaClient);
	      var transactionReceipt =  response.getReceipt(hederaClient);
	      return transactionReceipt;
	     
	}catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}
	    return null;
	}    
	    private static byte[] readContentIntoByteArray(File file)
	    {
	       FileInputStream fileInputStream = null;
	       byte[] bFile = new byte[(int) file.length()];
	       try
	       {
	          //convert file into array of bytes
	          fileInputStream = new FileInputStream(file);
	          fileInputStream.read(bFile);
	          fileInputStream.close();
	          for (int i = 0; i < bFile.length; i++)
	          {
	             System.out.print((char) bFile[i]);
	          }
	       }
	       catch (Exception e)
	       {
	          e.printStackTrace();
	       }
	       return bFile;
	    }
	    
	    public Client createHederaClient() throws InterruptedException {
	        var networkName = "testnet";
	        var mirrorNetwork ="hcs.testnet.mirrornode.hedera.com:5600";
	        System.out.println("mirrorNetwork....." + mirrorNetwork);
	        Client client;

	        // noinspection EnhancedSwitchMigration
	        switch (networkName) {
	            case "mainnet":
	                client = Client.forMainnet();

	                System.out.println("Create Hedera client for Mainnet");
	               
	                System.out.println(
	                    "Using {} to connect to the hedera mirror network" +
	                    mirrorNetwork
	                );
	                client.setMirrorNetwork(List.of(mirrorNetwork));
	                break;
	            case "testnet":
	                System.out.println("Create Hedera client for Testnet");

	                client = Client.forTestnet();
	                client.setMirrorNetwork(List.of(mirrorNetwork));
	                System.out.println( "Using {} to connect to the hedera mirror network"+mirrorNetwork);
	                System.out.println( "The hedera mirror network"+client.getMirrorNetwork());
	                System.out.println( "The network"+client.getNetwork());

	                break;
	            default:
	                throw new IllegalStateException(
	                    "unknown hedera network name: " + networkName
	                );
	        }

	        var operatorId = "0.0.4389";
	        System.out.println("operatorId {}"+ operatorId);
	        var operatorKey = "302e020100300506032b657004220420b56be9ea5c14be403fa44280fe85457ccbda9388e161b10ce1bfa072d1fcd2ab";
	        System.out.println("operatorKey {}"+operatorKey);

	        if (operatorId != null && operatorKey != null) {
	            client.setOperator(
	                AccountId.fromString(operatorId),
	                PrivateKey.fromString(operatorKey)
	            );
	        }
	       // logger.info("Topic ID ", client);

	        System.out.println("The OperatorPublicKey {}"+client.getOperatorPublicKey());

	        System.out.println("The OperatorAccountId {}"+ client.getOperatorAccountId());
	        System.out.println("HederaClinet....."+client);
	        return client;
	    }
}
