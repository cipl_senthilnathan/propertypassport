package com.propertypassport.web.hedera.service.impl;

import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.hedera.hashgraph.sdk.Client;
import com.hedera.hashgraph.sdk.PrivateKey;
import com.hedera.hashgraph.sdk.TopicId;
import com.hedera.hashgraph.sdk.TransactionReceipt;
import com.propertypassport.web.dataaccess.model.PropertyAddress;
import com.propertypassport.web.dataaccess.model.PropertyPassportInfo;
import com.propertypassport.web.dataaccess.model.User;
import com.propertypassport.web.dataaccess.payload.request.FileCreationDto;
import com.propertypassport.web.dataaccess.payload.response.MessageResponse;
import com.propertypassport.web.dataaccess.repository.PropertyAddressRepository;
import com.propertypassport.web.dataaccess.repository.PropertyPassportInfoRepository;
import com.propertypassport.web.dataaccess.repository.UserRepository;
import com.propertypassport.web.hedera.payload.request.NFTTokenDto;
import com.propertypassport.web.hedera.service.NFTTokenService;
import com.propertypassport.web.hedera.utils.FileServiceUtil;
import com.propertypassport.web.hedera.utils.HCSTransactionService;

@Service
public class NFTTokenServiceImpl implements NFTTokenService {

	@Autowired
	PropertyPassportInfoRepository nftTokenInfoRepository;

	@Autowired
	UserRepository userRepository;
	@Autowired
	PropertyAddressRepository propertyAddressRepository;
	@Autowired
	private Environment env;

	@Override
	public ResponseEntity<?> createNFTToken(NFTTokenDto nftTokenDto) {
		// TODO Auto-generated method stub
		TransactionReceipt transaction = null;
		try {
			var topicId = Optional.ofNullable(env.getProperty("H721_TOPIC")).map(TopicId::fromString).get();

			var nftToken = PrivateKey.generate();
			// nftTokenDto.setNftToken(nftToken);
			var publicKey = nftToken.getPublicKey();
			User user = userRepository.getById(nftTokenDto.getUser().getId());
			PropertyAddress propertyAddress = propertyAddressRepository
					.findById(nftTokenDto.getPropertyAddress().getId()).get();

			PropertyPassportInfo nftTokenInfo = PropertyPassportInfo.builder().nftTokenId(publicKey.toString())
					.userId(user).propertyName(nftTokenDto.getPropertyName()).tokenSymbol(nftTokenDto.getTokenSymbol())
					.addressId(propertyAddress).build();
			PropertyPassportInfo nftTokenInfo2 = nftTokenInfoRepository.save(nftTokenInfo);
			HCSTransactionService generateTopicId = new HCSTransactionService();
			transaction = generateTopicId.publishMessageToHedera(topicId, nftTokenInfo2);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		if (Objects.nonNull(transaction)) {
			return ResponseEntity.ok(new MessageResponse(HttpStatus.OK.value(),
					"Property Passport  Created and published into Mirror Node Successfully", transaction));
		} else {
			return ResponseEntity.ok(new MessageResponse(HttpStatus.PARTIAL_CONTENT.value(),
					"Error while  publishing Property Passport into Mirror Node", transaction));
		}

	}

	@Override
	public ResponseEntity<?> createFile(FileCreationDto fileCreationDto) throws InterruptedException {
		// TODO Auto-generated method stub
		String fileId = createNewFile(fileCreationDto);
		
		return ResponseEntity.ok(new MessageResponse(HttpStatus.OK.value(),
				"File Successfully", fileId));
	}

	public String createNewFile(FileCreationDto fileCreationDto) throws InterruptedException {
		// Create the new file and set its properties

		FileServiceUtil fileServiceUtil = new FileServiceUtil();
		Client client = fileServiceUtil.createHederaClient();
		String fileId = fileServiceUtil.fileCreate(fileCreationDto.getFileContent(), client,
				fileCreationDto.getOperatorKey());
		
		return fileId;
	}

}
