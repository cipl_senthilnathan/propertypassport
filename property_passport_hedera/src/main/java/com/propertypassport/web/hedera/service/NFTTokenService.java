package com.propertypassport.web.hedera.service;


import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.propertypassport.web.dataaccess.payload.request.FileCreationDto;
import com.propertypassport.web.hedera.payload.request.NFTTokenDto;

@Service
public interface NFTTokenService {

	public ResponseEntity<?> createNFTToken(NFTTokenDto nftTokenDto) ;
	
	public ResponseEntity<?> createFile(FileCreationDto fileCreationDto)throws InterruptedException;
}
