package com.propertypassport.web.hedera.payload.response;

import lombok.Data;

@Data
public class NFTResponse {

	private String fileId;
	private String  nftTokenId;
}
