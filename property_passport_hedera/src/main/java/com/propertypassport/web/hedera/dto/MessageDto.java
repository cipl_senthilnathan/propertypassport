package com.propertypassport.web.hedera.dto;

import org.joda.time.MutableDateTime;

import com.hedera.hashgraph.sdk.AccountId;
import com.hedera.hashgraph.sdk.TokenId;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MessageDto {

	private TokenId tokenId;
	private AccountId accountId;
	private MutableDateTime endDate;
	private Long reservePrice;
	private String ownerName;
	private String coOwnerName;
	private PropertyAddressDto propertyAddress;
	
}
