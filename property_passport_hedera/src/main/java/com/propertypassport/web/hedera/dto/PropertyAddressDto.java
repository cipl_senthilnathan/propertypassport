package com.propertypassport.web.hedera.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PropertyAddressDto {

	private String typeOfProperty;
	private String postalCode;
	private String building;
	private String town;
	private String country;
	
}
