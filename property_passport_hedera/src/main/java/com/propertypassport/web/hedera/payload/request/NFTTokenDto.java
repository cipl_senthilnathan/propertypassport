package com.propertypassport.web.hedera.payload.request;

import com.propertypassport.web.dataaccess.model.PropertyAddress;
import com.propertypassport.web.dataaccess.model.User;

import lombok.Data;

@Data
public class NFTTokenDto {
	private String propertyName;
	private String tokenSymbol;
	private User user;
	private PropertyAddress propertyAddress;

}
