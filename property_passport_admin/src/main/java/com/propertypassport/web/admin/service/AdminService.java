package com.propertypassport.web.admin.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface AdminService {

	public ResponseEntity<?> getProperty( String addressId);

	public ResponseEntity<?> adminApproval(String userId);	
	
	public ResponseEntity<?> propertyTransfer(String userId, String addressId, HttpServletRequest request);
}
