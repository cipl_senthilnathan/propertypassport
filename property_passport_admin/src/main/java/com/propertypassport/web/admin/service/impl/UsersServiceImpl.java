package com.propertypassport.web.admin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.propertypassport.web.admin.service.*;
import com.propertypassport.web.dataaccess.message.PaginationMessageResponse;
import com.propertypassport.web.dataaccess.model.User;
import com.propertypassport.web.dataaccess.repository.UserRepository;


@Service
public class UsersServiceImpl implements UsersService{

	
	@Autowired
	UserRepository userRepository;
	@Autowired
	private Environment env;
	
	
	@Override
	public ResponseEntity<?> getListUser(Pageable pageable) {
		
		Page<User> userList = userRepository.findAll(pageable);
		return ResponseEntity.ok( PaginationMessageResponse.builder().status(HttpStatus.OK.value()).message(env.getProperty("user.fetch.success"))
				.response(userList.getContent()).totalElements(userList.getTotalElements()).totalPages(userList.getTotalPages()).build() );
	}

}
