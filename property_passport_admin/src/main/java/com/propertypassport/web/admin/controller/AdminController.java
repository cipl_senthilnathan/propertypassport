package com.propertypassport.web.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.propertypassport.web.admin.service.AdminService;
import com.propertypassport.web.admin.service.UsersService;
import com.propertypassport.web.dataaccess.exception.FundTransferMirrorNodeException;
import com.propertypassport.web.dataaccess.exception.UserSecurityNotFoundException;

import io.swagger.v3.oas.annotations.parameters.RequestBody;
@CrossOrigin
@RestController
@RequestMapping(value = "/api/admin")
@PreAuthorize("hasRole('MANAGER') or hasRole('ADMIN')")
public class AdminController {
	@Autowired
	UsersService usersService;

	@Autowired
	AdminService adminService;

	@GetMapping("/user/all")
	public ResponseEntity<?> userList(@RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "size", defaultValue = "10") int size) {
		return usersService.getListUser(PageRequest.of(page, size, Sort.by(Direction.DESC, "username")));
	}
	
	@GetMapping("/property")
	public ResponseEntity<?> getProperty(@PathVariable(value = "addressId") String addressId){
		return adminService.getProperty(addressId);
	}
	
	
	@PostMapping("/kyc/approve/{userId}")
	public ResponseEntity<?> adminApproval(@PathVariable("userId") String userId) {
		return adminService.adminApproval(userId);
	}
	
	@PostMapping("/property/transfer")
	public ResponseEntity<?> propertyTransfer(@PathVariable(value = "userId") String userId,@PathVariable(value = "addressId") String addressId,
			HttpServletRequest request)throws FundTransferMirrorNodeException,UserSecurityNotFoundException {
		return adminService.propertyTransfer(userId, addressId, request);
	}
}
