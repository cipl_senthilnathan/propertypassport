package com.propertypassport.web.admin.service.impl;

import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.propertypassport.web.admin.service.AdminService;
import com.propertypassport.web.dataaccess.model.KycInfo;
import com.propertypassport.web.dataaccess.model.OwnerStatus;
import com.propertypassport.web.dataaccess.model.PropertyAddress;
import com.propertypassport.web.dataaccess.model.PropertyOwnerRelation;
import com.propertypassport.web.dataaccess.model.User;
import com.propertypassport.web.dataaccess.model.UserStatus;
import com.propertypassport.web.dataaccess.payload.response.MessageResponse;
import com.propertypassport.web.dataaccess.repository.KycInfoRepo;
import com.propertypassport.web.dataaccess.repository.PropertyAddressRepository;
import com.propertypassport.web.dataaccess.repository.PropertyOwnerRelationRepository;
import com.propertypassport.web.dataaccess.repository.UserRepository;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	PropertyAddressRepository propertyAddressRepository;

	@Autowired
	KycInfoRepo kycInfoRepo;

	@Autowired
	Environment env;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	PropertyOwnerRelationRepository propertyOwnerRelationRepository;

	@Override
	public ResponseEntity<?> getProperty(String addressId) {
		// TODO Auto-generated method stub
		PropertyAddress propertyAddress=propertyAddressRepository.findById(addressId).get();
		return ResponseEntity.ok(new MessageResponse(HttpStatus.OK.value(),
				"File Successfully", propertyAddress));
	}

	@Override
	public ResponseEntity<?> adminApproval(String userId) {
		User user=userRepository.getById(userId);
		KycInfo kyc=kycInfoRepo.findByUserId(user);
		if(kyc!=null)
			kyc.setStatus(UserStatus.APPROVED);
		kycInfoRepo.save(kyc);
		return ResponseEntity.ok(new MessageResponse(HttpStatus.OK.value(),
				env.getProperty("admin.approve.success"), kyc));
	}

	
	@Override
	public ResponseEntity<?> propertyTransfer(String userId, String addressId, HttpServletRequest request) {
		// TODO Auto-generated method stub
		User user = userRepository.getById(userId);
		PropertyAddress propertyAddress = propertyAddressRepository.findById(addressId).get();
		PropertyOwnerRelation propertyOwnerRelation = PropertyOwnerRelation.builder().addressId(propertyAddress)
				.userId(user).propertyOwnerStatus(OwnerStatus.ACTIVE).createdTime(new DateTime()).build();
		
		propertyOwnerRelationRepository.save(propertyOwnerRelation);
		
		return null;
	}


}
