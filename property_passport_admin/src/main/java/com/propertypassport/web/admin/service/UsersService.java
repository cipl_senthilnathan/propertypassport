package com.propertypassport.web.admin.service;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface UsersService {

	ResponseEntity<?> getListUser(Pageable pageable);

}
