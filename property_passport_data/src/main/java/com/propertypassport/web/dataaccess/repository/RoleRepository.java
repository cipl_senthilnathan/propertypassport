package com.propertypassport.web.dataaccess.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.propertypassport.web.dataaccess.model.ERole;
import com.propertypassport.web.dataaccess.model.Role;



@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(ERole name);
	
	Boolean existsByName(ERole name);
	
}
