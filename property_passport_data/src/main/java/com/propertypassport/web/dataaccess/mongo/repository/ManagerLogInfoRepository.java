package com.propertypassport.web.dataaccess.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.propertypassport.web.dataaccess.mongo.model.ManagerLogInfo;

@Repository
public interface ManagerLogInfoRepository extends MongoRepository<ManagerLogInfo, Long> {

}
