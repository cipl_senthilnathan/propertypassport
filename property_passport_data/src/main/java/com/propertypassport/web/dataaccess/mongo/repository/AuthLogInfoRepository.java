package com.propertypassport.web.dataaccess.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.propertypassport.web.dataaccess.mongo.model.AuthLogInfo;

@Repository
public interface AuthLogInfoRepository extends MongoRepository<AuthLogInfo, Long>{

}
