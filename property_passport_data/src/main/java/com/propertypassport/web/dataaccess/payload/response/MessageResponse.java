package com.propertypassport.web.dataaccess.payload.response;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class MessageResponse {
	
	private int status;
	private String message;
	private Object response;
	
	public MessageResponse(String string,int status) {
		this.message = string;
		this.status = status;
	}
}
