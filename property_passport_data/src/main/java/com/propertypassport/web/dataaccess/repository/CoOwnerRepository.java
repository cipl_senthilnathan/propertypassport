package com.propertypassport.web.dataaccess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.propertypassport.web.dataaccess.model.CoOwner;
import com.propertypassport.web.dataaccess.model.PropertyAddress;
import com.propertypassport.web.dataaccess.model.User;

@Repository
public interface CoOwnerRepository extends JpaRepository<CoOwner, String>,PagingAndSortingRepository<CoOwner, String>{

	@Query("select co from CoOwner co where co.userId=:user and co.propertyAddressId=:address")
	CoOwner getCoOwner(@Param("user") User user,@Param("address") PropertyAddress address);

}
