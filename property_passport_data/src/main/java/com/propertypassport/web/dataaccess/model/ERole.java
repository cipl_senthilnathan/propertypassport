package com.propertypassport.web.dataaccess.model;

public enum ERole {
	ROLE_USER,
	ROLE_MANAGER,
    ROLE_ADMIN
}
