package com.propertypassport.web.dataaccess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.propertypassport.web.dataaccess.model.PropertyAddress;
import com.propertypassport.web.dataaccess.model.PropertyPassportInfo;
import com.propertypassport.web.dataaccess.model.User;

@Repository
public interface PropertyPassportInfoRepository extends JpaRepository<PropertyPassportInfo, String>,PagingAndSortingRepository<PropertyPassportInfo, String>{

	@Query("select ppi.nftTokenId from PropertyPassportInfo ppi where ppi.userId=:user and addressId=:address")
	PropertyPassportInfo getTokenId(@Param("user") User user,@Param("address")PropertyAddress address);
}
