package com.propertypassport.web.dataaccess.common.request;

import com.propertypassport.web.dataaccess.model.PropertyAddress;
import com.propertypassport.web.dataaccess.model.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class NFTTokenDto {
	private String propertyName;
	private String tokenSymbol;
	private User user;
	private PropertyAddress propertyAddress;
}
