package com.propertypassport.web.dataaccess.config;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.boot.actuate.mongo.MongoHealthIndicator;

@Component
public class PowertransitionMongoHealthIndicator extends MongoHealthIndicator{

	public PowertransitionMongoHealthIndicator(MongoTemplate mongoTemplate) {
		super(mongoTemplate);
	}
}
