package com.propertypassport.web.dataaccess.mongo.model;

public enum LogLevel {
	INFO, ERROR
}
