package com.propertypassport.web.dataaccess.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.propertypassport.web.dataaccess.mongo.model.HederaLogInfo;

@Repository
public interface HederaLogInfoRepository extends MongoRepository<HederaLogInfo, Long> {

}
