package com.propertypassport.web.dataaccess.mongo.model;

public enum SeverityType {
	Informational, Alert, Error, Critical
}
