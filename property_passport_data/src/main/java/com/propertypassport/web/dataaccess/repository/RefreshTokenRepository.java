package com.propertypassport.web.dataaccess.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.propertypassport.web.dataaccess.model.RefreshToken;
import com.propertypassport.web.dataaccess.model.User;

public interface RefreshTokenRepository extends JpaRepository<RefreshToken, String> {

	@Override
	Optional<RefreshToken> findById(String id);

	Optional<RefreshToken> findByToken(String token);

	int deleteByUser(User byId);

}
