package com.propertypassport.web.dataaccess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.propertypassport.web.dataaccess.model.KycInfo;
import com.propertypassport.web.dataaccess.model.User;


@Repository
public interface KycInfoRepo extends JpaRepository<KycInfo, String>{

	KycInfo findByUserId(User user);

	



}
