package com.propertypassport.web.dataaccess.exception;

import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.google.gson.Gson;
import com.propertypassport.web.dataaccess.common.response.PowertransitionMessageResponse;

@ControllerAdvice
public class PowertransitionExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	Environment env;

	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		return buildResponseEntity(
				new ErrorMessage(HttpStatus.BAD_REQUEST, new DateTime(), env.getProperty("bad.credentials"), ""));
	}

	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException userNotFoundException) {
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.PARTIAL_CONTENT.value()).message(env.getProperty("user.not.found"))),
				HttpStatus.PARTIAL_CONTENT);
	}

	@ExceptionHandler(ResourceNotFoundException.class)
	protected ResponseEntity<Object> handleResourceNotFound(ResourceNotFoundException ex) {
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.BAD_REQUEST.value()).message(env.getProperty("resource.not.found"))),
				HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handle(Exception ex, HttpServletResponse response) {
		ex.printStackTrace();
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).message(env.getProperty("internal.server.error"))),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(PathNotFoundException.class)
	public ResponseEntity<Object> handleHttpMessageNotReadableException(PathNotFoundException ex,
			HttpServletResponse response) {
		ex.printStackTrace();
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.NOT_FOUND.value()).message(env.getProperty("path.not.found"))),
				HttpStatus.NOT_FOUND);
	}

	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.BAD_REQUEST.value()).message(env.getProperty("url.not.found"))),
				HttpStatus.BAD_REQUEST);
	}

	private ResponseEntity<Object> buildResponseEntity(ErrorMessage apiError) {
		return new ResponseEntity<>(apiError, apiError.getHttpStatus());
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		return new ResponseEntity<>(
				new Gson().toJson(PowertransitionMessageResponse.builder().status(HttpStatus.PARTIAL_CONTENT.value())
						.message(String.format("%s has invalid value %s",
								ex.getBindingResult().getFieldError().getField(),
								ex.getBindingResult().getFieldError().getRejectedValue()))),
				HttpStatus.PARTIAL_CONTENT);
	}

	@ExceptionHandler(RoleNotFoundException.class)
	public ResponseEntity<Object> handleRoleNotFoundException(RoleNotFoundException roleNotFoundException) {
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.PARTIAL_CONTENT.value()).message(env.getProperty("role.not.found"))),
				HttpStatus.PARTIAL_CONTENT);
	}

	@ExceptionHandler(BadCredentialsException.class)
	public ResponseEntity<Object> handleBadCrentialsException(BadCredentialsException badCredentialsException) {
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.PARTIAL_CONTENT.value()).message(env.getProperty("bad.credentials"))),
				HttpStatus.PARTIAL_CONTENT);
	}

	@ExceptionHandler(UsernameAlreadyExistsException.class)
	public ResponseEntity<Object> handleUsernameAlreadyExistsException(
			UsernameAlreadyExistsException usernameAlreadyExistsException) {
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.PARTIAL_CONTENT.value()).message(env.getProperty("username.exist"))),
				HttpStatus.PARTIAL_CONTENT);
	}

	@ExceptionHandler(BalanceTokenNotFoundException.class)
	public ResponseEntity<Object> handleBalanceTokenNotFoundException(
			BalanceTokenNotFoundException balanceTokenNotFoundException) {
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.PARTIAL_CONTENT.value()).message(env.getProperty("balance.token.not.found"))),
				HttpStatus.PARTIAL_CONTENT);
	}

	@ExceptionHandler(UserSecurityNotFoundException.class)
	public ResponseEntity<Object> handleUserSecurityNotFoundException(
			UserSecurityNotFoundException userSecurityNotFoundException) {
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.PARTIAL_CONTENT.value()).message(env.getProperty("user.security.not.found"))),
				HttpStatus.PARTIAL_CONTENT);
	}

	@ExceptionHandler(PublishingMirrorNodeException.class)
	public ResponseEntity<Object> handlePublishingMirrorNodeException(
			PublishingMirrorNodeException publishingMirrorNodeException) {
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.PARTIAL_CONTENT.value()).message(env.getProperty("publishing.mirror.node.error"))),
				HttpStatus.PARTIAL_CONTENT);
	}

	@ExceptionHandler(EvBatteryStatusException.class)
	public ResponseEntity<Object> handleEvBatteryStatusException(
			EvBatteryStatusException evBatteryStatusException) {
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.PARTIAL_CONTENT.value()).message(env.getProperty("evbattery.status.error"))),
				HttpStatus.PARTIAL_CONTENT);
	}

	@ExceptionHandler(UserInfoException.class)
	public ResponseEntity<Object> handleUserInfoException(UserInfoException userInfoException) {
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.PARTIAL_CONTENT.value()).message(env.getProperty("user.info.error"))),
				HttpStatus.PARTIAL_CONTENT);
	}

	@ExceptionHandler(TradeMirrorNodeException.class)
	public ResponseEntity<Object> handleTradeMirrorNodeException(TradeMirrorNodeException tradeMirrorNodeException) {
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.PARTIAL_CONTENT.value()).message(env.getProperty("trade.mirror.node.error"))),
				HttpStatus.PARTIAL_CONTENT);
	}

	@ExceptionHandler(CreateWalletMirrorNodeException.class)
	public ResponseEntity<Object> handleCreateWalletMirrorNodeException(
			CreateWalletMirrorNodeException createWalletMirrorNodeException) {
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.PARTIAL_CONTENT.value()).message(env.getProperty("wallet.mirror.node.error"))),
				HttpStatus.PARTIAL_CONTENT);
	}

	@ExceptionHandler(GetBalanceMirrorNodeException.class)
	public ResponseEntity<Object> handleGetBalanceMirrorNodeException(
			GetBalanceMirrorNodeException getBalanceMirrorNodeException) {
		return new ResponseEntity<>(new Gson().toJson(PowertransitionMessageResponse.builder()
				.status(HttpStatus.PARTIAL_CONTENT.value()).message(env.getProperty("get.balance.mirror.node.error"))),
				HttpStatus.PARTIAL_CONTENT);
	}

	@ExceptionHandler(InitialTokenMirrorNodeException.class)
	public ResponseEntity<Object> handleInitialTokenMirrorNodeException(
			InitialTokenMirrorNodeException initialTokenMirrorNodeException) {
		return new ResponseEntity<>(
				new Gson().toJson(PowertransitionMessageResponse.builder().status(HttpStatus.PARTIAL_CONTENT.value())
						.message(env.getProperty("initial.token.mirror.node.error"))),
				HttpStatus.PARTIAL_CONTENT);
	}

	@ExceptionHandler(FundTransferMirrorNodeException.class)
	public ResponseEntity<Object> handleFundTransferMirrorNodeException(
			FundTransferMirrorNodeException fundTransferMirrorNodeException) {
		return new ResponseEntity<>(
				new Gson().toJson(PowertransitionMessageResponse.builder().status(HttpStatus.PARTIAL_CONTENT.value())
						.message(env.getProperty("fund.transfer.mirror.node.error"))),
				HttpStatus.PARTIAL_CONTENT);
	}

	@ExceptionHandler(value = TokenRefreshException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public ErrorMessage handleTokenRefreshException(TokenRefreshException ex, WebRequest request) {
		return new ErrorMessage(HttpStatus.FORBIDDEN, new DateTime(), ex.getMessage(), request.getDescription(false));
	}
}
