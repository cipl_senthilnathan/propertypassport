package com.propertypassport.web.dataaccess.common.request;

import lombok.Data;

@Data
public class PropertyAddressRequest {

	private String postalCode;
	private String building;
	private String town;
	private String country;
	private String userId;
}
