package com.propertypassport.web.dataaccess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.propertypassport.web.dataaccess.model.User;
import com.propertypassport.web.dataaccess.model.UserSecurity;

@Repository
public interface UserSecurityRepository extends JpaRepository<UserSecurity, Long>,PagingAndSortingRepository<UserSecurity, Long>{

	UserSecurity getByUserId( User id );

}
