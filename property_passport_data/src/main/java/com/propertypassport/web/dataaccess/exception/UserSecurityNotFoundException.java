package com.propertypassport.web.dataaccess.exception;

public class UserSecurityNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 8307506986388742263L;
}
