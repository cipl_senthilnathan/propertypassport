package com.propertypassport.web.dataaccess.constant;

public class PropertyPassportConstant {

	public static final String PUBLISH_MIRROR_NODE_USER = "/publishmirronode/user";
	public static final String PUBLISH_MIRROR_NODE = "/publishmirronode";
	public static final String GET_BALANCE_API = "/balance"; 
	public static final String CREATE_NFT_TOKEN = "/createNftToken"; 
	public static final String INITIAL_TOKEN = "/associatkycuser";
	public static final String FUND_TRANSFER ="/transfer";
	public static final double MAX_VALUE = 100;
	public static final String CREATE_WALLET_API = "/createwallet"; 
	public static final String CREATE_TOKEN="/createtoken";
	public static final String ACCOUNT_SID = "AC47ce9597a9c219b0908145ff2620e9be";
	public static final String AUTH_TOKEN = "5c364b94e7a41f8d78b9d9bd896c3550";
	public static final String phoneNumber ="(479)888-7605";
	public static final String create_nft_token="/createNFTToken";
	public static final String create_file="/createFile";
}
