package com.propertypassport.web.dataaccess.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class PaginationMessageResponse {

	public int status;
	public String message;
	public Object response;
	public long  totalElements;
	public int totalPages;
}
