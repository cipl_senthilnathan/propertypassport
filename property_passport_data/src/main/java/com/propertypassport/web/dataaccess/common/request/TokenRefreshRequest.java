package com.propertypassport.web.dataaccess.common.request;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class TokenRefreshRequest {
	
	@NotBlank
	  private String refreshToken;

}
