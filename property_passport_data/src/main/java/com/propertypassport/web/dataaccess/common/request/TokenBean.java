package com.propertypassport.web.dataaccess.common.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
	 * TokenBean class used to receive inputs and pass to service 
	 * receive inputs values with the help of getters and setters
	 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TokenBean {

	private int amount;
	private String operatorId;
	private String operatorKey;
	private String tokenId;
	private String fromSenderId;
	private String fromSenderKey;
	private String toAccountId;
	private String toAccountKey;
}
