package com.propertypassport.web.dataaccess.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.propertypassport.web.dataaccess.model.UserInfo;

@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo, String>,PagingAndSortingRepository<UserInfo, String>{

	Optional<UserInfo> findById(String userId);

	UserInfo getById(String userId);
}
