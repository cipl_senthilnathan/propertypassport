package com.propertypassport.web.dataaccess.payload.request;

import lombok.Data;

@Data
public class NFTTokenRequest {
	private String fileId;
	private String operatorId;
	private String operatorKey;
}
