package com.propertypassport.web.dataaccess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.propertypassport.web.dataaccess.model.PropertyDetails;

@Service
public interface PropertyDetailsRepo extends JpaRepository<PropertyDetails, String> {

}
