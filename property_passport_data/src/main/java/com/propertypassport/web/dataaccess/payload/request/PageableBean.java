package com.propertypassport.web.dataaccess.payload.request;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PageableBean {

	@NotBlank
	private int page;

	@NotBlank
	private int size;
	
}
