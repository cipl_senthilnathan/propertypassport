package com.propertypassport.web.dataaccess.common.request;

import lombok.Data;

@Data
public class PropertyPassportRequest {

	private String userId;
	private String propertyName;
	private String tokenSymbol;
	private String addressId;
}
