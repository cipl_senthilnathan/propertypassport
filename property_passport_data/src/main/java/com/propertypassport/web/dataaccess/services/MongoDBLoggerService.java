package com.propertypassport.web.dataaccess.services;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Service;

import com.propertypassport.web.dataaccess.common.request.LogRequest;
import com.propertypassport.web.dataaccess.config.PowertransitionMongoHealthIndicator;
import com.propertypassport.web.dataaccess.mongo.model.LogLevel;
import com.propertypassport.web.dataaccess.mongo.model.ServiceType;
import com.propertypassport.web.dataaccess.mongo.model.SeverityType;

@Service
public class MongoDBLoggerService {

	@Autowired
	public  PowertransitionMongoHealthIndicator mongoHealthIndicator;
	
	@Autowired
	public  MongoService mongoService;
	
	public  void createLogger(String message,ServiceType serviceType) {
		if (mongoHealthIndicator.health().getStatus() == Status.UP) {
			mongoService.saveLogInformation(LogRequest.builder().severity(SeverityType.Alert)
					.logLevel(LogLevel.INFO).logInfo(message)
					.createdTime(new DateTime()).type(serviceType).build());
		} else {
			mongoService.saveLogInformation(LogRequest.builder().severity(SeverityType.Alert)
					.logLevel(LogLevel.INFO).logInfo(message)
					.createdTime(new DateTime()).type(ServiceType.LOG).build());
		}
	}
}
