package com.propertypassport.web.dataaccess.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.propertypassport.web.dataaccess.model.User;
import com.propertypassport.web.dataaccess.payload.request.PageableBean;
import com.propertypassport.web.dataaccess.repository.UserRepository;

@Service
public class RequestHandlerService {
	@Autowired
	UserRepository userRepository;
	
	public Page<User> userList( PageableBean pageableBean ) {
		Pageable pageable = PageRequest.of( pageableBean.getPage(), pageableBean.getSize(),Sort.by( Direction.DESC,"username" ) );
		return userRepository.findAll( pageable );
		//return null;
	}
	
}
