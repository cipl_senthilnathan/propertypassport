package com.propertypassport.web.dataaccess.model;

public enum OwnerStatus {
	ACTIVE,
	IN_ACTIVE
}
