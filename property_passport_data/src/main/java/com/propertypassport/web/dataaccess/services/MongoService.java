package com.propertypassport.web.dataaccess.services;

import java.util.List;

import com.propertypassport.web.dataaccess.common.request.LogRequest;
import com.propertypassport.web.dataaccess.common.response.LogResponse;
import com.propertypassport.web.dataaccess.mongo.model.ServiceType;


public interface MongoService {
	
	public String getExceptionToString(Exception exception);
	
	public void saveLogInformation(LogRequest logRequest);
	
	public List<LogResponse> fetchLogInformations(ServiceType serviceType);

}
