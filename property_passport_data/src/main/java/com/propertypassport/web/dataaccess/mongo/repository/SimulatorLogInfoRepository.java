package com.propertypassport.web.dataaccess.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.propertypassport.web.dataaccess.mongo.model.SimulatorLogInfo;

@Repository
public interface SimulatorLogInfoRepository extends MongoRepository<SimulatorLogInfo, Long> {

}
