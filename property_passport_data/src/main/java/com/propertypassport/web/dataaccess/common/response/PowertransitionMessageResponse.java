package com.propertypassport.web.dataaccess.common.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PowertransitionMessageResponse {

	private int status;

	private String message;

	private Object result;
	
	
}
