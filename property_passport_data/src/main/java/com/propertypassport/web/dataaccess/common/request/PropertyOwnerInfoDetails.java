package com.propertypassport.web.dataaccess.common.request;

import java.util.List;

import com.propertypassport.web.dataaccess.model.CoOwner;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class PropertyOwnerInfoDetails {

	private String ownerName;
	private String nftTokenId;
	private String fileId;
	private String propertyName;
	private String propertyTokenSymbol;
	private String postalCode;
	private String town;
	private String country;
	private List<CoOwner> coOwners;
}
