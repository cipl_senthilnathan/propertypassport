package com.propertypassport.web.dataaccess.common.request;

import lombok.Data;

@Data
public class CoOwnerRequest  {

	private String ownershipShare;
	private String addressId;
	private String userId;
	private String firstName;
	private String lastName;
	private String emailAddress;
	private String mobileNumber;
}
