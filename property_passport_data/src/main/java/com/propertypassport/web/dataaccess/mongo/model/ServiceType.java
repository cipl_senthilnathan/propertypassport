package com.propertypassport.web.dataaccess.mongo.model;

public enum ServiceType {
	DATA, AUTH, ADMIN,HCS,HEDERA,HTS,MANAGER,REPORT,SIMULATOR,USER, LOG
}
