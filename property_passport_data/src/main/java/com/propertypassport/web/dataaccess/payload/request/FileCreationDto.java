package com.propertypassport.web.dataaccess.payload.request;

import lombok.Data;

@Data
public class FileCreationDto {

	private byte[] fileContent;
	private String operatorId;
	private String operatorKey;
}
