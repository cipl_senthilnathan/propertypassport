package com.propertypassport.web.dataaccess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.propertypassport.web.dataaccess.model.PropertyOwnerRelation;

@Repository
public interface PropertyOwnerRelationRepository extends JpaRepository<PropertyOwnerRelation, String>,PagingAndSortingRepository<PropertyOwnerRelation, String>{

}
