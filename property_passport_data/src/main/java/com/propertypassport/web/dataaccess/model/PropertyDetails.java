package com.propertypassport.web.dataaccess.model;



import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;

import lombok.Builder;
import lombok.Data;

@Entity
@Table(name = "property_passport_details")
@Data
@Builder
public class PropertyDetails {
	@Id
	@GenericGenerator(name = "uuid-gen", strategy = "uuid2")
	@GeneratedValue(generator = "uuid-gen")
	private String id;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id", nullable = false)
	private User userId;

	private String typeof_property;

	private String tokenId;

	private int bedrooms;

	private int bathrooms;

	private DateTime createdTime;

	private int surfaceArea;

	private String floorPlanPath;

	private String energyReportFilePath;

	private String validationReportFilePath;

	private String fileId;

	private String addressId;

	private String tokenSymbol;

	private String propertyName;
	
	@Transient
	private String methodName;

}
