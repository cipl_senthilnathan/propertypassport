package com.propertypassport.web.dataaccess.payload.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FundTransferRequest {

	private int amount;
	private String fromSenderId;
	private String fromSenderKey;
	private String accountId;
	private String tokenId;

}
