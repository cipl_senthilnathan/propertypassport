package com.propertypassport.web.dataaccess.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.propertypassport.web.dataaccess.model.Role;
import com.propertypassport.web.dataaccess.model.User;


@Repository
public interface UserRepository extends JpaRepository<User, Long>,PagingAndSortingRepository<User, Long> {
	Optional<User> findByUsername(String username);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);
	
	Page<User> findAll( Pageable pageable );
	
	List<User> findAll( );

	User getById(String id);

	Page<User> findById(String id,Pageable pageable);
	
	Boolean existsByPassword(String password);
	
	User findByEmail(String email);
	
	List<User> findAllByRoles(Role role);

	Optional<User> findById(String userId);
}
