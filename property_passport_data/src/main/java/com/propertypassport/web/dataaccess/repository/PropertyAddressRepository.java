package com.propertypassport.web.dataaccess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.propertypassport.web.dataaccess.model.PropertyAddress;
import com.propertypassport.web.dataaccess.model.User;

@Repository
public interface PropertyAddressRepository extends JpaRepository<PropertyAddress, String>,PagingAndSortingRepository<PropertyAddress, String>{

	PropertyAddress findByUserId(User user);

}
