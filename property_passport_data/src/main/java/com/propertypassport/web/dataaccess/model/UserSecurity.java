package com.propertypassport.web.dataaccess.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(	name = "user_security")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserSecurity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GenericGenerator(name = "uuid-gen", strategy = "uuid2")
	@GeneratedValue(generator = "uuid-gen")
	private String id;
	
	
	@OneToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User userId;
	
	@NotNull
	private long accountShard;
	
	@NotNull
	private long accountRealm;
	
	@NotNull
	private long accountNum;
	
	@NotBlank
	private String userPubKey;
	
	@NotBlank
	private String userPrivateKey;
	
	private String accountBalance;

//	@OneToOne
//	@JoinColumn(name = "address_id", nullable = false)
//	private PropertyAddress addressId;
	
	@Transient
	private String methodName;
}
