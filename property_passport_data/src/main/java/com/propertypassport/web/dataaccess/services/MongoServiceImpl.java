package com.propertypassport.web.dataaccess.services;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.propertypassport.web.dataaccess.common.request.LogRequest;
import com.propertypassport.web.dataaccess.common.response.LogResponse;
import com.propertypassport.web.dataaccess.mongo.model.AdminLogInfo;
import com.propertypassport.web.dataaccess.mongo.model.AuthLogInfo;
import com.propertypassport.web.dataaccess.mongo.model.DataLogInfo;
import com.propertypassport.web.dataaccess.mongo.model.HcsLogInfo;
import com.propertypassport.web.dataaccess.mongo.model.HederaLogInfo;
import com.propertypassport.web.dataaccess.mongo.model.HtsLogInfo;
import com.propertypassport.web.dataaccess.mongo.model.LogLevel;
import com.propertypassport.web.dataaccess.mongo.model.ManagerLogInfo;
import com.propertypassport.web.dataaccess.mongo.model.ReportLogInfo;
import com.propertypassport.web.dataaccess.mongo.model.ServiceType;
import com.propertypassport.web.dataaccess.mongo.model.SimulatorLogInfo;
import com.propertypassport.web.dataaccess.mongo.model.UserLogInfo;
import com.propertypassport.web.dataaccess.mongo.repository.AdminLogInfoRepository;
import com.propertypassport.web.dataaccess.mongo.repository.AuthLogInfoRepository;
import com.propertypassport.web.dataaccess.mongo.repository.DataLogInfoRepository;
import com.propertypassport.web.dataaccess.mongo.repository.HcsLogInfoRepository;
import com.propertypassport.web.dataaccess.mongo.repository.HederaLogInfoRepository;
import com.propertypassport.web.dataaccess.mongo.repository.HtsLogInfoRepository;
import com.propertypassport.web.dataaccess.mongo.repository.ManagerLogInfoRepository;
import com.propertypassport.web.dataaccess.mongo.repository.ReportLogInfoRepository;
import com.propertypassport.web.dataaccess.mongo.repository.SimulatorLogInfoRepository;
import com.propertypassport.web.dataaccess.mongo.repository.UserLogInfoRepository;
import com.propertypassport.web.dataaccess.services.MongoService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MongoServiceImpl implements MongoService {

	@Autowired
	private DataLogInfoRepository dataLogInfoRepository;

	@Autowired
	private AuthLogInfoRepository authLogInfoRepository;
	
	@Autowired
	private AdminLogInfoRepository adminLogInfoRepository;
	
	@Autowired
	private HcsLogInfoRepository hcsLogInfoRepository;
	
	@Autowired
	private HederaLogInfoRepository hederaLogInfoRepository;
	
	@Autowired
	private HtsLogInfoRepository htsLogInfoRepository;
	
	@Autowired
	private ManagerLogInfoRepository managerLogInfoRepository;
	
	@Autowired
	private ReportLogInfoRepository reportLogInfoRepository;
	
	@Autowired
	private SimulatorLogInfoRepository simulatorLogInfoRepository;
	
	@Autowired
	private UserLogInfoRepository userLogInfoRepository;

	@Override
	public String getExceptionToString(Exception exception) {
		StringWriter errors = new StringWriter();
		exception.printStackTrace(new PrintWriter(errors));
		return errors.toString();
	}

	@Async
	@Override
	public void saveLogInformation(LogRequest logRequest) {
		ServiceType type;
		type = logRequest.getType();
		switch (type) {
		case AUTH:
			authLogInfoRepository
					.save(AuthLogInfo.builder().severity(logRequest.getSeverity()).logLevel(logRequest.getLogLevel())
							.logInfo(logRequest.getLogInfo()).createdTime(logRequest.getCreatedTime()).build());
			break;

		case DATA:
			dataLogInfoRepository
					.save(DataLogInfo.builder().severity(logRequest.getSeverity()).logLevel(logRequest.getLogLevel())
							.logInfo(logRequest.getLogInfo()).createdTime(logRequest.getCreatedTime()).build());
			break;
		
		case ADMIN:
			adminLogInfoRepository
					.save(AdminLogInfo.builder().severity(logRequest.getSeverity()).logLevel(logRequest.getLogLevel())
							.logInfo(logRequest.getLogInfo()).createdTime(logRequest.getCreatedTime()).build());
			break;
			
		case HCS:
			hcsLogInfoRepository
					.save(HcsLogInfo.builder().severity(logRequest.getSeverity()).logLevel(logRequest.getLogLevel())
							.logInfo(logRequest.getLogInfo()).createdTime(logRequest.getCreatedTime()).build());
			break;
			
		case HEDERA:
			hederaLogInfoRepository
					.save(HederaLogInfo.builder().severity(logRequest.getSeverity()).logLevel(logRequest.getLogLevel())
							.logInfo(logRequest.getLogInfo()).createdTime(logRequest.getCreatedTime()).build());
			break;
			
		case HTS:
			htsLogInfoRepository
					.save(HtsLogInfo.builder().severity(logRequest.getSeverity()).logLevel(logRequest.getLogLevel())
							.logInfo(logRequest.getLogInfo()).createdTime(logRequest.getCreatedTime()).build());
			break;
			
		case MANAGER:
			managerLogInfoRepository
					.save(ManagerLogInfo.builder().severity(logRequest.getSeverity()).logLevel(logRequest.getLogLevel())
							.logInfo(logRequest.getLogInfo()).createdTime(logRequest.getCreatedTime()).build());
			break;
			
		case REPORT:
			reportLogInfoRepository
					.save(ReportLogInfo.builder().severity(logRequest.getSeverity()).logLevel(logRequest.getLogLevel())
							.logInfo(logRequest.getLogInfo()).createdTime(logRequest.getCreatedTime()).build());
			break;
			
		case SIMULATOR:
			simulatorLogInfoRepository
					.save(SimulatorLogInfo.builder().severity(logRequest.getSeverity()).logLevel(logRequest.getLogLevel())
							.logInfo(logRequest.getLogInfo()).createdTime(logRequest.getCreatedTime()).build());
			break;
			
		case USER:
			userLogInfoRepository
					.save(UserLogInfo.builder().severity(logRequest.getSeverity()).logLevel(logRequest.getLogLevel())
							.logInfo(logRequest.getLogInfo()).createdTime(logRequest.getCreatedTime()).build());
			break;

		default:
			if (LogLevel.INFO == logRequest.getLogLevel()) {
				log.info(logRequest.getLogInfo());
			} else {
				log.error(logRequest.getLogInfo());
			}
			break;
		}

	}

	@Override
	public List<LogResponse> fetchLogInformations(ServiceType serviceType) {
		ServiceType type;
		type = serviceType;
		switch (type) {
		case AUTH:
			return authLogInfoRepository.findAll().stream()
					.sorted(Comparator.comparing(AuthLogInfo::getCreatedTime).reversed())
					.map(auth -> getAuthLogResponse(auth)).collect(Collectors.toList());
		case DATA:
			return dataLogInfoRepository.findAll().stream()
					.sorted(Comparator.comparing(DataLogInfo::getCreatedTime).reversed())
					.map(data -> getDataLogResponse(data)).collect(Collectors.toList());
		default:
			return null;
		}
	}

	private LogResponse getAuthLogResponse(AuthLogInfo authLogInfo) {
		return LogResponse.builder().id(authLogInfo.getId()).severity(authLogInfo.getSeverity())
				.logLevel(authLogInfo.getLogLevel()).logInfo(authLogInfo.getLogInfo())
				.createdTime(authLogInfo.getCreatedTime()).build();
	}


	private LogResponse getDataLogResponse(DataLogInfo dataLogInfo) {
		return LogResponse.builder().id(dataLogInfo.getId()).severity(dataLogInfo.getSeverity())
				.logLevel(dataLogInfo.getLogLevel()).logInfo(dataLogInfo.getLogInfo())
				.createdTime(dataLogInfo.getCreatedTime()).build();
	}


}
