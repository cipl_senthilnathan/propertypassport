DROP TABLE IF EXISTS roles;
CREATE TABLE public.roles
(
    id text NOT NULL,
    name text,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.roles
    OWNER to postgres;
    
DROP TABLE IF EXISTS users;

CREATE TABLE public.users
(
    id text NOT NULL,
    email text DEFAULT NULL,
    password text DEFAULT NULL,
    username text DEFAULT NULL,
    mobile_number numeric NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT email_unique_key UNIQUE (email),
    CONSTRAINT username_unique_key UNIQUE (username)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.users
    OWNER to postgres;

CREATE TABLE public.user_roles
(
    user_id text NOT NULL,
    role_id text NOT NULL,
    PRIMARY KEY (user_id, role_id),
    CONSTRAINT user_foreign_key FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT role_foreign_key FOREIGN KEY (role_id)
        REFERENCES public.roles (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.user_roles
    OWNER to postgres;