 
-- Table: public.user_info

-- DROP TABLE public.user_info;

CREATE TABLE public.user_info
(
    user_id text COLLATE pg_catalog."default" NOT NULL,
    first_name text COLLATE pg_catalog."default",
    last_name text COLLATE pg_catalog."default",
    email_address text COLLATE pg_catalog."default",
    mobile_number numeric(15,0),
    validate_otp numeric(10,0),
    CONSTRAINT user_info_pkey PRIMARY KEY (user_id),
    CONSTRAINT user_id_foreign_key FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.user_info
    OWNER to postgres;

-- Table: public.property_address

-- DROP TABLE public.property_address;

CREATE TABLE public.property_address
(
    id text COLLATE pg_catalog."default" NOT NULL,
    postal_code text COLLATE pg_catalog."default",
    building text COLLATE pg_catalog."default",
    town text COLLATE pg_catalog."default",
    country text COLLATE pg_catalog."default",
    user_id text COLLATE pg_catalog."default",
    max_co_owner text COLLATE pg_catalog."default",
    created_time timestamp without time zone,
    updated_time timestamp without time zone,
    CONSTRAINT property_address_pkey PRIMARY KEY (id),
    CONSTRAINT property_address_user_fkey FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public.property_address
    OWNER to postgres;
    
-- Table: public.property_passport_info

-- DROP TABLE public.property_passport_info;

CREATE TABLE public.property_passport_info
(
    id text COLLATE pg_catalog."default" NOT NULL,
    nft_token_id text COLLATE pg_catalog."default",
    property_name text COLLATE pg_catalog."default",
    token_symbol text COLLATE pg_catalog."default",
    user_id text COLLATE pg_catalog."default",
    address_id text COLLATE pg_catalog."default",
    CONSTRAINT property_passport_info_pkey PRIMARY KEY (id),
    CONSTRAINT nft_token_user_fkey FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT property_passport_address FOREIGN KEY (address_id)
        REFERENCES public.property_address (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public.property_passport_info
    OWNER to postgres;
    
-- Table: public.co_onwer

-- DROP TABLE public.co_onwer;

CREATE TABLE public.co_owner
(
    id text COLLATE pg_catalog."default" NOT NULL,
    user_id text COLLATE pg_catalog."default" NOT NULL,
    first_name text COLLATE pg_catalog."default",
    last_name text COLLATE pg_catalog."default",
    email text COLLATE pg_catalog."default",
    mobile text COLLATE pg_catalog."default",
    ownership_share text COLLATE pg_catalog."default",
    property_address_id text COLLATE pg_catalog."default",
    CONSTRAINT co_onwer_pkey PRIMARY KEY (id),
    CONSTRAINT co_owner_property_address_fkey FOREIGN KEY (property_address_id)
        REFERENCES public.property_address (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT owner_user_fkey FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.co_owner
    OWNER to postgres;
    
-- Table: public.user_security

-- DROP TABLE public.user_security;

CREATE TABLE public.user_security
(
    user_id text COLLATE pg_catalog."default" NOT NULL,
    account_shard numeric NOT NULL,
    account_realm numeric NOT NULL,
    account_num numeric NOT NULL,
    user_pub_key text COLLATE pg_catalog."default" NOT NULL,
    user_private_key text COLLATE pg_catalog."default" NOT NULL,
    account_balance text COLLATE pg_catalog."default",
    address_id text COLLATE pg_catalog."default",
    id text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT user_security_pkey PRIMARY KEY (id),
    CONSTRAINT user_id_sec_foreign_key FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT user_security_address_fkey FOREIGN KEY (address_id)
        REFERENCES public.property_address (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public.user_security
    OWNER to postgres;