CREATE TABLE public.property_owner_relation
(
    id text COLLATE pg_catalog."default" NOT NULL,
    address_id text COLLATE pg_catalog."default" NOT NULL,
    user_id text COLLATE pg_catalog."default" NOT NULL,
    created_time timestamp without time zone NOT NULL,
    property_owner_status text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT property_owner_relation_pkey PRIMARY KEY (id),
    CONSTRAINT owner_address_fkey FOREIGN KEY (address_id)
        REFERENCES public.property_address (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT owner_user_fkey FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.property_owner_relation
    OWNER to postgres;