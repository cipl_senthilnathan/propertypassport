CREATE TABLE public.kyc_info
(
    id text NOT NULL,
    user_id text NOT NULL,
    document_type text,
    document_number text,
    file_path text,
    status text,
   created_time timestamp without time zone,
    PRIMARY KEY (id),
    CONSTRAINT kyc_info_fkey FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
TABLESPACE pg_default;

ALTER TABLE public.kyc_info
    OWNER to postgres;
    
    
    
    CREATE TABLE public.property_passport_details
(
    id text NOT NULL,
    user_id text NOT NULL,
    typeof_property text,
    bedrooms  numeric,
    bathrooms numeric,
    surface_area numeric,
    floor_plan_path text,
    energy_report_file_path text,
    validation_report_file_path text,
     token_id text,
   created_time timestamp without time zone,
    CONSTRAINT property_passport_details_pkey PRIMARY KEY (id),
    CONSTRAINT property_passport_details_fkey FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
TABLESPACE pg_default;

ALTER TABLE public.property_passport_details
    OWNER to postgres;
    
    
    
    