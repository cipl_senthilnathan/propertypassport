CREATE TABLE public.refresh_token
(
    id text NOT NULL,
    user_id text NOT NULL,
    token text,
    expiry_date time without time zone,
    PRIMARY KEY (id),
    CONSTRAINT userkey FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.refresh_token
    OWNER to postgres;