package com.propertypassport.web.user.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.propertypassport.web.dataaccess.common.request.CoOwnerRequest;
import com.propertypassport.web.dataaccess.common.request.PropertyAddressRequest;
import com.propertypassport.web.dataaccess.common.request.PropertyPassportRequest;
import com.propertypassport.web.dataaccess.exception.FundTransferMirrorNodeException;
import com.propertypassport.web.dataaccess.exception.PublishingMirrorNodeException;
import com.propertypassport.web.dataaccess.exception.UserSecurityNotFoundException;
import com.propertypassport.web.user.payload.request.KycDetailsRequest;
import com.propertypassport.web.user.service.UsersService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/user")
@PreAuthorize("hasRole('USER')")
public class UserController {

	@Autowired
	UsersService usersService;

	@PostMapping("propertpassport/create")
	public ResponseEntity<?> createPropertyPassport(@RequestBody PropertyPassportRequest propertyPassportRequest,
			HttpServletRequest request) {
		return usersService.createPropertyPassport(propertyPassportRequest, request);
	}

	@PostMapping(value = "/createwallet/{userId}/{initialToken}")
	public ResponseEntity<?> createWallet(@PathVariable(value = "userId") String userId,
			//@PathVariable(value = "addressId") String addressId, 
			@PathVariable("initialToken") int initialToken,HttpServletRequest request) throws PublishingMirrorNodeException {
		return usersService.createWallet(userId, initialToken, request);
	}
	
	@PostMapping(value = "/createpropertylocation")
	public ResponseEntity<?> createPropertyLocation(@RequestBody PropertyAddressRequest propertyAddressRequest ){
		return usersService.createPropertyLocation(propertyAddressRequest);
	}
	
	@PostMapping(value = "/insert/coowner/info")
	public ResponseEntity<?> insertCoOwnerPersonalDetails(@RequestBody CoOwnerRequest coOwnerRequest ){
		return usersService.insertCoOwnerPersonalDetails(coOwnerRequest);
	}
	

	@GetMapping(value = "/coowner/info/{userId}/{addressId}")
	public ResponseEntity<?> getCoOwnerInfo(@PathVariable(value = "userId") String userId,
			@PathVariable(value = "addressId") String addressId){
		return usersService.getCoOwnerInfo(userId,addressId);
	}
	
	@GetMapping(value = "/getbalance/{userId}")
	public ResponseEntity<?> getBalance(@PathVariable(value = "userId") String userId,HttpServletRequest request){
		return usersService.getBalance(userId, request);
	}
	
	@PostMapping("/transfer/fund")
	public ResponseEntity<?> fundransfer(@PathVariable(value = "userId") String userId,@PathVariable("initialToken") int initialToken,
			HttpServletRequest request)throws FundTransferMirrorNodeException,UserSecurityNotFoundException {
		return usersService.fundtransfer(userId, initialToken, request);
	}
	
	@GetMapping("/property")
	public ResponseEntity<?> getProperty(@PathVariable(value = "addressId") String addressId){
		return usersService.getProperty(addressId);
	}
	@RequestMapping(value = "kyc/info/uploadfile", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "Title Deed Validation ", notes = "Need to save KYC Details")
	public ResponseEntity<?> kycUpload(
			@ApiParam(value = "Required kycInfo details", required = true) @RequestParam(name = "kycInfo", value = "kycInfo", required = true) String kycInfo,
			@ApiParam(value = "Required file attachment", required = true) @RequestParam(name = "documentType", value = "documentType", required = true) MultipartFile documentType)
			
					throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		KycDetailsRequest kycdetails=null;
		
		try {
			kycdetails = mapper.readValue(kycInfo, KycDetailsRequest.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return usersService.saveKycInfo(kycdetails, documentType);
	}
	
	@RequestMapping(value = "property/details/uploadfile ", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "Title Deed Validation ", notes = "Need to save property Details")
	public ResponseEntity<?> passportProprt(HttpServletRequest request,
			@ApiParam(value = "Required property details", required = true) @RequestParam(name = "kycInfo", value = "kycInfo", required = true) String kycInfo,
			@ApiParam(value = "Required file attachment", required = true) @RequestParam(name = "floorPlan", value = "floorPlan", required = false) MultipartFile floorPlan,
			@ApiParam(value = "Required files attachment", required = true) @RequestParam(name = "energyReport", value = "energyReport", required = true) MultipartFile energyReport,
			@ApiParam(value = "Required files attachment description", required = true) @RequestParam(name = "valuationReport", value = "valuationReport", required = true) MultipartFile valuationReport)
					throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		KycDetailsRequest kycdetails=null;
		
		try {
			kycdetails = mapper.readValue(kycInfo, KycDetailsRequest.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return usersService.savePropertyInfo(kycdetails, floorPlan,energyReport,valuationReport,request);
	}
	
	@PostMapping(value="/property/address/save")
	public ResponseEntity<?> propertyAddressSave(@RequestBody PropertyAddressRequest propertyAddressRequest){
		return usersService.propetyPassportSave(propertyAddressRequest);
	}
}
