package com.propertypassport.web.user.service;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.propertypassport.web.dataaccess.common.request.CoOwnerRequest;
import com.propertypassport.web.dataaccess.common.request.PropertyAddressRequest;
import com.propertypassport.web.dataaccess.common.request.PropertyPassportRequest;
import com.propertypassport.web.dataaccess.exception.FundTransferMirrorNodeException;
import com.propertypassport.web.dataaccess.exception.PublishingMirrorNodeException;
import com.propertypassport.web.dataaccess.exception.UserSecurityNotFoundException;
import com.propertypassport.web.user.payload.request.KycDetailsRequest;

@Service
public interface UsersService {
	public ResponseEntity<?> createPropertyPassport(PropertyPassportRequest propertyPassportRequest,
			HttpServletRequest request);

	public ResponseEntity<?> createPropertyLocation(PropertyAddressRequest propertyAddressRequest);

	public ResponseEntity<?> createWallet(String userId, int initialToken, HttpServletRequest request) throws PublishingMirrorNodeException;

	public ResponseEntity<?> insertCoOwnerPersonalDetails(CoOwnerRequest coOwnerRequest);

	public ResponseEntity<?> getRegistrantInfo(String userId);

	public ResponseEntity<?> getCoOwnerInfo(String userId, String addressId);

	public ResponseEntity<?> getBalance(String userId, HttpServletRequest request);

	public ResponseEntity<?> fundtransfer(String userId, int initialToken, HttpServletRequest request)
			throws FundTransferMirrorNodeException, UserSecurityNotFoundException;

	public ResponseEntity<?> getProperty(String addressId);

	public ResponseEntity<?> saveKycInfo(KycDetailsRequest kycdetails, MultipartFile documentType) throws IOException;

	public ResponseEntity<?> savePropertyInfo(KycDetailsRequest kycdetails, MultipartFile floorPlan,
			MultipartFile energyReport, MultipartFile valuationReport,HttpServletRequest request) throws IOException, PublishingMirrorNodeException;

	public ResponseEntity<?> propetyPassportSave(PropertyAddressRequest propertyAddressRequest);

}
