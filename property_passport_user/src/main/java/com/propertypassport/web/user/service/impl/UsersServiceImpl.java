package com.propertypassport.web.user.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.propertypassport.web.dataaccess.common.request.CoOwnerRequest;
import com.propertypassport.web.dataaccess.common.request.NFTTokenDto;
import com.propertypassport.web.dataaccess.common.request.PropertyAddressRequest;
import com.propertypassport.web.dataaccess.common.request.PropertyPassportRequest;
import com.propertypassport.web.dataaccess.common.request.TokenBean;
import com.propertypassport.web.dataaccess.constant.PropertyPassportConstant;
import com.propertypassport.web.dataaccess.exception.FundTransferMirrorNodeException;
import com.propertypassport.web.dataaccess.exception.GetBalanceMirrorNodeException;
import com.propertypassport.web.dataaccess.exception.PublishingMirrorNodeException;
import com.propertypassport.web.dataaccess.exception.UserSecurityNotFoundException;
import com.propertypassport.web.dataaccess.model.CoOwner;
import com.propertypassport.web.dataaccess.model.KycInfo;
import com.propertypassport.web.dataaccess.model.PropertyAddress;
import com.propertypassport.web.dataaccess.model.PropertyDetails;
import com.propertypassport.web.dataaccess.model.PropertyPassportInfo;
import com.propertypassport.web.dataaccess.model.User;
import com.propertypassport.web.dataaccess.model.UserInfo;
import com.propertypassport.web.dataaccess.model.UserSecurity;
import com.propertypassport.web.dataaccess.model.UserStatus;
import com.propertypassport.web.dataaccess.mongo.model.ServiceType;
import com.propertypassport.web.dataaccess.payload.request.FileCreationDto;
import com.propertypassport.web.dataaccess.payload.request.NFTTokenRequest;
import com.propertypassport.web.dataaccess.payload.response.MessageResponse;
import com.propertypassport.web.dataaccess.repository.CoOwnerRepository;
import com.propertypassport.web.dataaccess.repository.KycInfoRepo;
import com.propertypassport.web.dataaccess.repository.PropertyAddressRepository;
import com.propertypassport.web.dataaccess.repository.PropertyDetailsRepo;
import com.propertypassport.web.dataaccess.repository.PropertyPassportInfoRepository;
import com.propertypassport.web.dataaccess.repository.UserInfoRepository;
import com.propertypassport.web.dataaccess.repository.UserRepository;
import com.propertypassport.web.dataaccess.repository.UserSecurityRepository;
import com.propertypassport.web.dataaccess.services.MongoDBLoggerService;
import com.propertypassport.web.dataaccess.services.RestTemplateService;
import com.propertypassport.web.user.payload.request.KycDetailsRequest;
import com.propertypassport.web.user.payload.response.WalletCreateResponse;
import com.propertypassport.web.user.service.UsersService;

@Service
@Transactional
public class UsersServiceImpl implements UsersService {

	@Autowired
	RestTemplateService restTemplateService;

	@Value("${nft.url}")
	private String nftBaseUrl;

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserInfoRepository userInfoRespository;

	@Autowired
	private Environment env;

	@Autowired
	PropertyPassportInfoRepository propertyPassportInfoRepository;

	@Autowired
	PropertyAddressRepository propertyAddressRepository;
	private RestTemplate restTemplate = new RestTemplate();

	@Value("${charge.pertime}")
	private String chargePerTime;

	@Autowired
	MongoDBLoggerService mongoDBLoggerService;

	@Autowired
	CoOwnerRepository coOnwerRepository;

	@Autowired
	KycInfoRepo kycRepository;

	@Autowired
	PropertyDetailsRepo propertyDetailsRepo;

	@Value("${hcs.url}")
	private String hcsBaseUrl;

	@Value("${hedera.url}")
	private String hederaBaseUrl;

	@Value("${hts.url}")
	private String htsBaseUrl;

	@Autowired
	UserSecurityRepository userSecurityRepository;

	@Value("${admin.accountId}")
	private String adminAccountId;

	@Value("${admin.accountKey}")
	private String adminAccountKey;

	@Value("${pound.per.token}")
	private int poundPerToken;

	@Value("${mobile.app.tokenid}")
	private String mobileTokenId;

	private static final Logger LOG = LoggerFactory.getLogger(UsersServiceImpl.class);
	private static FileWriter file;

	@Override
	public ResponseEntity<?> createPropertyPassport(PropertyPassportRequest propertyPassportRequest,
			HttpServletRequest request) {
		// TODO Auto-generated method stub
		User user = userRepository.getById(propertyPassportRequest.getUserId());
		Optional<PropertyAddress> propertyAddress = propertyAddressRepository
				.findById(propertyPassportRequest.getAddressId());

		NFTTokenDto nftToken = NFTTokenDto.builder().propertyName(propertyPassportRequest.getPropertyName())
				.tokenSymbol(propertyPassportRequest.getTokenSymbol()).user(user).propertyAddress(propertyAddress.get())
				.build();
		MessageResponse response = createNFTTokenInHedera(request, nftToken);
		return ResponseEntity.ok(response);
	}

	private MessageResponse createNFTTokenInHedera(HttpServletRequest request, NFTTokenDto nftTokenDto) {
		MessageResponse response = null;
		try {
			// after saving into the table,publish message to the server
			// String url1 = nftBaseUrl +
			// String.format(PowertransitionConstant.CREATE_NFT_TOKEN);
			String url1 = "http://localhost:9088/hedera/api/hedera/createNftToken";
			RequestEntity<NFTTokenDto> nftEntity = new RequestEntity<>(nftTokenDto,
					restTemplateService.headers(request), HttpMethod.POST, restTemplateService.uri(url1));
			response = restTemplate.exchange(nftEntity, MessageResponse.class).getBody();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseEntity<?> createPropertyLocation(PropertyAddressRequest propertyAddressRequest) {
		// TODO Auto-generated method stub
		User user = userRepository.getById(propertyAddressRequest.getUserId());
		PropertyAddress propertyAddress = PropertyAddress.builder().building(propertyAddressRequest.getBuilding())
				.postalCode(propertyAddressRequest.getPostalCode()).town(propertyAddressRequest.getTown())
				.country(propertyAddressRequest.getCountry()).userId(user).build();

		PropertyAddress propertyAddressResult = propertyAddressRepository.save(propertyAddress);
		return ResponseEntity.ok(new MessageResponse(HttpStatus.OK.value(), env.getProperty("property.address.success"),
				propertyAddressResult));
	}

	@Override
	public ResponseEntity<?> createWallet(String userId, int initialToken, HttpServletRequest request)
			throws PublishingMirrorNodeException {
		// TODO Auto-generated method stub
		MessageResponse response = null;
		UserSecurity userSecurity = new UserSecurity();
		try {
			response = createWalletInHedera(request);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		if (response.getStatus() == HttpStatus.OK.value()) {
			LinkedHashMap linkedHashMap = (LinkedHashMap) response.getResponse();
			if (Objects.nonNull(linkedHashMap)) {

				User user = userRepository.getById(userId);
				String balance = "0";
				// associate token
				String account = String.valueOf(linkedHashMap.get("account"));
				String privatekey = String.valueOf(linkedHashMap.get("privatekey"));
				String publickey = String.valueOf(linkedHashMap.get("publickey"));
				Long shard = new Long(linkedHashMap.get("shard").toString());
				Long real = new Long(linkedHashMap.get("real").toString());
				String accountIdStr = linkedHashMap.get("accoundId").toString();
				Long accoundId = new Long(accountIdStr);

				WalletCreateResponse walletCreateResponse = WalletCreateResponse.builder().account(account)
						.privatekey(privatekey).publickey(publickey).shard(shard).real(real).accoundId(accoundId)
						.build();
				userSecurity = userSecurityRepository.getByUserId(user);
				if (Objects.isNull(userSecurity)) {
					userSecurity = insertUserSecurity(walletCreateResponse, user, balance);
				}
				MessageResponse response1 = publishMessageToMirrorNode(request, walletCreateResponse);
			}
		}

		return ResponseEntity.ok(
				new MessageResponse(HttpStatus.OK.value(), env.getProperty("wallet.created.success"), userSecurity));
	}

	private MessageResponse publishMessageToMirrorNode(HttpServletRequest request,
			WalletCreateResponse walletCreateResponse) throws PublishingMirrorNodeException {
		String hcsUrl = hcsBaseUrl + String.format(PropertyPassportConstant.PUBLISH_MIRROR_NODE);
		MessageResponse response = null;
		try {
			walletCreateResponse.setMethodName("createWallet");
			RequestEntity<?> secEntity = new RequestEntity<>(walletCreateResponse, restTemplateService.headers(request),
					HttpMethod.POST, restTemplateService.uri(hcsUrl));
			response = restTemplate.exchange(secEntity, MessageResponse.class).getBody();

		} catch (Exception e) {

			throw new PublishingMirrorNodeException();
		}
		return response;
	}

	private MessageResponse publishMessageToHTS(HttpServletRequest request, TokenBean tokenBean) {
		MessageResponse response = null;
		try {
			String url1 = htsBaseUrl + String.format(PropertyPassportConstant.INITIAL_TOKEN);
			RequestEntity<TokenBean> secEntity = new RequestEntity<>(tokenBean, restTemplateService.headers(request),
					HttpMethod.POST, restTemplateService.uri(url1));
			response = restTemplate.exchange(secEntity, MessageResponse.class).getBody();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return response;
	}

	@Override
	public ResponseEntity<?> insertCoOwnerPersonalDetails(CoOwnerRequest coOwnerRequest) {
		// TODO Auto-generated method stub
		User user = userRepository.getById(coOwnerRequest.getUserId());
		Optional<PropertyAddress> propertyAddress = propertyAddressRepository.findById(coOwnerRequest.getAddressId());

		CoOwner coOwner = CoOwner.builder().firstName(coOwnerRequest.getFirstName())
				.lastName(coOwnerRequest.getLastName()).email(coOwnerRequest.getEmailAddress())
				.mobile(coOwnerRequest.getMobileNumber()).userId(user).propertyAddressId(propertyAddress.get())
				.ownershipShare(coOwnerRequest.getOwnershipShare()).build();

		CoOwner coOwnerSuccess = coOnwerRepository.save(coOwner);

		return ResponseEntity.ok(new MessageResponse(HttpStatus.OK.value(),
				env.getProperty("coowner.personal.info.success"), coOwnerSuccess));
	}

	@Override
	public ResponseEntity<?> getRegistrantInfo(String userId) {
		// TODO Auto-generated method stub
		User user = userRepository.getById(userId);
		Optional<UserInfo> userInfo = userInfoRespository.findById(user.getId());
		return ResponseEntity.ok(new MessageResponse(HttpStatus.OK.value(),
				env.getProperty("registrant.personal.fetch.success"), userInfo));
	}

	@Override
	public ResponseEntity<?> getCoOwnerInfo(String userId, String addressId) {
		// TODO Auto-generated method stub
		User user = userRepository.getById(userId);
		PropertyAddress propertyAddress = propertyAddressRepository.findById(addressId).get();
		CoOwner coOwner = coOnwerRepository.getCoOwner(user, propertyAddress);

		return ResponseEntity.ok(
				new MessageResponse(HttpStatus.OK.value(), env.getProperty("coowner.details.fetch.success"), coOwner));
	}

	private MessageResponse createWalletInHedera(HttpServletRequest request) {
		MessageResponse response = null;
		try {
			// after saving into the table,publish message to the server
			String url = hederaBaseUrl + String.format(PropertyPassportConstant.CREATE_WALLET_API);
			RequestEntity<?> entity = new RequestEntity<>(restTemplateService.headers(request), HttpMethod.POST,
					restTemplateService.uri(url));
			response = restTemplate.exchange(entity, MessageResponse.class).getBody();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return response;
	}

	private UserSecurity insertUserSecurity(WalletCreateResponse walletCreateResponse, User user, String balance) {
		UserSecurity userSecurity = null;
		try {
			PropertyAddress propertyAddress = propertyAddressRepository.findById(walletCreateResponse.getAddressId())
					.get();
			userSecurity = UserSecurity.builder().userId(user).userPubKey(walletCreateResponse.getPublickey())
					.userPrivateKey(walletCreateResponse.getPrivatekey()).accountShard(walletCreateResponse.getShard())
					.accountRealm(walletCreateResponse.getReal()).accountNum(walletCreateResponse.getAccoundId())
					// .addressId(propertyAddress)
					.accountBalance(balance).build();

			userSecurity = userSecurityRepository.save(userSecurity);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error while inserting user security........" + e.getMessage());
		}

		return userSecurity;
	}

	private MessageResponse publishMessageToHCS(HttpServletRequest request, UserSecurity userSecurity) {
		MessageResponse response = null;
		try {
			// after saving into the table,publish message to the server
			String url1 = hcsBaseUrl + String.format(PropertyPassportConstant.PUBLISH_MIRROR_NODE);
			userSecurity.setMethodName("usersecurity");
			RequestEntity<UserSecurity> secEntity = new RequestEntity<>(userSecurity,
					restTemplateService.headers(request), HttpMethod.POST, restTemplateService.uri(url1));
			response = restTemplate.exchange(secEntity, MessageResponse.class).getBody();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return response;
	}

	private String getTokenId(User user, String addressId) {

		String tokenId = "";
		try {
			PropertyAddress propertyAddress = propertyAddressRepository.findById(addressId).get();
			PropertyPassportInfo propertyPassportInfo = propertyPassportInfoRepository.getTokenId(user,
					propertyAddress);
			tokenId = propertyPassportInfo.getNftTokenId();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return tokenId;
	}

	@Override
	public ResponseEntity<?> getBalance(String userId, HttpServletRequest request) {
		// TODO Auto-generated method stub
		User user = userRepository.getById(userId);
		UserSecurity userSecurity = userSecurityRepository.getByUserId(user);
		MessageResponse response = null;
		if (Objects.nonNull(userSecurity)) {

			try {
				response = getBalance(request, userSecurity);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

		} else {
			return ResponseEntity
					.ok(new MessageResponse(HttpStatus.BAD_REQUEST.value(), env.getProperty("wallet.not.found"), user));
		}
		return ResponseEntity.ok(response);
	}

	private MessageResponse getBalance(HttpServletRequest request, UserSecurity userSecurity)
			throws GetBalanceMirrorNodeException {
		MessageResponse response = null;
		try {
			String url = htsBaseUrl + String.format(PropertyPassportConstant.GET_BALANCE_API);
			String accountId = userSecurity.getAccountShard() + "." + userSecurity.getAccountRealm() + "."
					+ userSecurity.getAccountNum();
			RequestEntity<String> entity = new RequestEntity<>(accountId, restTemplateService.headers(request),
					HttpMethod.POST, restTemplateService.uri(url));
			response = restTemplate.exchange(entity, MessageResponse.class).getBody();
		} catch (Exception e) {
			// TODO: handle exception
			throw new GetBalanceMirrorNodeException();
		}

		return response;
	}

	@Override
	@Transactional(rollbackFor = FundTransferMirrorNodeException.class)
	public ResponseEntity<?> fundtransfer(String userId, int initialToken, HttpServletRequest request)
			throws FundTransferMirrorNodeException, UserSecurityNotFoundException {
		User user = userRepository.getById(userId);
		UserSecurity userSec = userSecurityRepository.getByUserId(user);
		String acountId;
		if (Objects.nonNull(userSec)) {
			acountId = userSec.getAccountShard() + "." + userSec.getAccountRealm() + "." + userSec.getAccountNum();
		} else {
			return ResponseEntity.ok(new MessageResponse(HttpStatus.BAD_REQUEST.value(),
					env.getProperty("user.security.not.found"), userSec));
		}

		int amount = (int) (initialToken * poundPerToken);
		System.out.println("Amount..." + amount);
		TokenBean tokenBean = TokenBean.builder().fromSenderId(adminAccountId).fromSenderKey(adminAccountKey)
				.toAccountId(acountId).tokenId(mobileTokenId).amount(amount).build();

		MessageResponse response = null;
		try {
			response = fundTransfer(request, tokenBean);
		} catch (FundTransferMirrorNodeException e) {
			// TODO: handle exception
			mongoDBLoggerService.createLogger(env.getProperty("fund.transfer.mirror.node.error"), ServiceType.USER);
			throw new FundTransferMirrorNodeException();
		}
		if (response.getStatus() == HttpStatus.OK.value()) {
			LinkedHashMap linkedHashMap = (LinkedHashMap) response.getResponse();
			String balance = String.valueOf(linkedHashMap.get("balanceRemain"));
			userSec.setAccountBalance(balance);
			userSecurityRepository.save(userSec);

		} else {
			return ResponseEntity.ok(response);
		}

		return ResponseEntity
				.ok(new MessageResponse(HttpStatus.OK.value(), env.getProperty("fundtransfer.success"), userSec));
	}

	private MessageResponse fundTransfer(HttpServletRequest request, TokenBean tokenBean)
			throws FundTransferMirrorNodeException {
		MessageResponse response = null;
		try {
			String url1 = htsBaseUrl + String.format(PropertyPassportConstant.FUND_TRANSFER);
			RequestEntity<TokenBean> secEntity = new RequestEntity<>(tokenBean, restTemplateService.headers(request),
					HttpMethod.POST, restTemplateService.uri(url1));
			response = restTemplate.exchange(secEntity, MessageResponse.class).getBody();
		} catch (Exception e) {
			// TODO: handle exception
			throw new FundTransferMirrorNodeException();
		}

		return response;
	}

	@Override
	public ResponseEntity<?> getProperty(String addressId) {
		// TODO Auto-generated method stub
		PropertyAddress propertyAddress = propertyAddressRepository.findById(addressId).get();
		return ResponseEntity.ok(new MessageResponse(HttpStatus.OK.value(), "File Successfully", propertyAddress));
	}

	@Override
	public ResponseEntity<?> saveKycInfo(KycDetailsRequest kycdetails, MultipartFile documentType) throws IOException {

		User user = userRepository.getById(kycdetails.getUserId());
//			 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
//			   LocalDateTime now = LocalDateTime.now(); 
		if (user != null) {
			String filePath = "";
			filePath = userDocumentUpload(documentType, kycdetails.getUserId(), "documentType");

			if (filePath == null) {
				return ResponseEntity.ok(new MessageResponse(HttpStatus.BAD_REQUEST.value(),
						env.getProperty("upload.require.failure"), ""));
			}
			KycInfo kycInfo = KycInfo.builder().documentType(kycdetails.getDocumentType())
					.documentNumber(kycdetails.getDocumentNumber()).status(UserStatus.UNAPPROVED).filePath(filePath)
					.userId(user).build();
			kycInfo = kycRepository.save(kycInfo);
		}
		return ResponseEntity
				.ok(new MessageResponse(HttpStatus.OK.value(), env.getProperty("upload.require.success"), ""));
	}

	private String userDocumentUpload(MultipartFile documentType, String userId, String docType) throws IOException {
		FileInputStream reader = null;
		FileOutputStream writer = null;
		String path = null;
		String dbPath = null;
		String userRegId = "0";

		LOG.info("In  userDocumentUpload : start");
		try {
			Optional<User> optionalUser = userRepository.findById(userId);
			User user = optionalUser.get();
			if (user != null) {
				userRegId = user.getId();
			}
			LOG.info(" Base path from config table : " + env.getProperty("spring.http.multipart.location"));
			String uploadingdir = env.getProperty("spring.http.multipart.location") + File.separator
					+ env.getProperty("user.document.location") + File.separator + userRegId + File.separator + docType;
			LOG.info(" uploadingdir : " + uploadingdir);
			File file = new File(uploadingdir);

			if (file.exists()) {
				file.delete();
			}
			if (!file.exists()) {
				LOG.info(" In mkdir : " + uploadingdir);
				file.mkdirs();
			}
			LOG.info(" uploadingdir : " + uploadingdir);

			String fileType = Files.getFileExtension(documentType.getOriginalFilename());
			String fileName = Files.getNameWithoutExtension(documentType.getOriginalFilename());

			if (fileType.equalsIgnoreCase("jpeg") || fileType.equalsIgnoreCase("png")
					|| fileType.equalsIgnoreCase("jpg") || fileType.equalsIgnoreCase("pdf")) {

				path = uploadingdir + File.separator + fileName + "." + fileType;
				dbPath = File.separator + env.getProperty("user.document.location") + File.separator + userRegId
						+ File.separator + docType + File.separator + fileName + "." + fileType;
				LOG.info(" file path : " + path);
				LOG.info("dbPath : " + dbPath);
				byte[] buffer = new byte[1000];
				File outputFile = new File(path);

				int totalBytes = 0;
				outputFile.createNewFile();
				reader = (FileInputStream) documentType.getInputStream();
				writer = new FileOutputStream(outputFile);

				int bytesRead = 0;
				while ((bytesRead = reader.read(buffer)) != -1) {
					writer.write(buffer);
					totalBytes += bytesRead;
				}
				dbPath = dbPath.replace(File.separator, "/");
				LOG.info("totalBytes:::" + totalBytes);

				reader.close();
				writer.close();
			} else {
				path = null;
				dbPath = null;
			}

		} catch (IOException e) {
			path = null;
			dbPath = null;
			reader.close();
			writer.close();
			LOG.error("Problem in userDocumentUpload file path : " + path);
			e.printStackTrace();
		} finally {

		}
		LOG.info("In  userDocumentUpload : end : dbPath : " + dbPath);
		return dbPath;

	}

	@Override
	public ResponseEntity<?> savePropertyInfo(KycDetailsRequest kycdetails, MultipartFile floorPlan,
			MultipartFile energyReport, MultipartFile valuationReport, HttpServletRequest request)
			throws IOException, PublishingMirrorNodeException {
		User user = userRepository.getById(kycdetails.getUserId());
		PropertyAddress adderss = propertyAddressRepository.findByUserId(user);
		MessageResponse response = null;
		MessageResponse createNftToken = null;
		if (user != null) {
			String floorPlanFilePath = "", energyReportFilePath = "", valuationReportFilePath = "";
			floorPlanFilePath = userDocumentUpload(floorPlan, kycdetails.getUserId(), "floorPlan");
			energyReportFilePath = userDocumentUpload(energyReport, kycdetails.getUserId(), "energyReport");
			valuationReportFilePath = userDocumentUpload(valuationReport, kycdetails.getUserId(), "valuationReport");
			if (floorPlanFilePath == null || energyReportFilePath == null || valuationReportFilePath == null) {
				return ResponseEntity.ok(new MessageResponse(HttpStatus.BAD_REQUEST.value(),
						env.getProperty("upload.require.failure"), ""));
			}
			PropertyDetails kycInfo = PropertyDetails.builder().bedrooms(kycdetails.getBedrooms())
					.floorPlanPath(floorPlanFilePath).validationReportFilePath(valuationReportFilePath)
					.energyReportFilePath(energyReportFilePath).surfaceArea(kycdetails.getSurfaceArea()).userId(user)
					.addressId(adderss.getId()).build();
			String json = new Gson().toJson(kycInfo);

			String convertFile = convertJsonToFile(json);

			byte[] arr = convertFile.getBytes();

			UserSecurity secutity = userSecurityRepository.getByUserId(user);
			String walletId = getWalletId(secutity);
			FileCreationDto createFileId = insertInfo(arr, walletId, secutity);
			response = createFileIdService(createFileId, request);
			NFTTokenRequest nft = createNFT(response, walletId, secutity);
			createNftToken = nftTokenCreation(nft, request);
			kycInfo.setFileId(response.getResponse().toString());
			kycInfo.setTokenId(createNftToken.getResponse().toString());
			MessageResponse publishHcs = publishMessageToHCS(request, kycInfo);
			kycInfo = propertyDetailsRepo.save(kycInfo);
		}
		return ResponseEntity
				.ok(new MessageResponse(HttpStatus.OK.value(), env.getProperty("upload.require.success"), ""));
	}

	private MessageResponse publishMessageToHCS(HttpServletRequest request, PropertyDetails kycInfo)
			throws PublishingMirrorNodeException {
		String hcsUrl = hcsBaseUrl + String.format(PropertyPassportConstant.PUBLISH_MIRROR_NODE);
		MessageResponse response = null;
		try {
			kycInfo.setMethodName("personalInfo");
			RequestEntity<?> secEntity = new RequestEntity<>(kycInfo, restTemplateService.headers(request),
					HttpMethod.POST, restTemplateService.uri(hcsUrl));
			response = restTemplate.exchange(secEntity, MessageResponse.class).getBody();

		} catch (Exception e) {

			throw new PublishingMirrorNodeException();
		}
		return response;
	}

	private MessageResponse nftTokenCreation(NFTTokenRequest nft, HttpServletRequest request) {
		String url1 = htsBaseUrl + String.format(PropertyPassportConstant.create_nft_token);
		RequestEntity<NFTTokenRequest> secEntity = new RequestEntity<>(nft, restTemplateService.headers(request),
				HttpMethod.POST, restTemplateService.uri(url1));
		MessageResponse response = restTemplate.exchange(secEntity, MessageResponse.class).getBody();

		return response;
	}

	private NFTTokenRequest createNFT(MessageResponse response, String walletId, UserSecurity secutity) {
		NFTTokenRequest nft = new NFTTokenRequest();
		nft.setFileId(response.getResponse().toString());
		nft.setOperatorId(walletId);
		nft.setOperatorKey(secutity.getUserPrivateKey());
		return nft;
	}

	private MessageResponse createFileIdService(FileCreationDto createFileId, HttpServletRequest request) {
		String url1 = hederaBaseUrl + String.format(PropertyPassportConstant.create_file);
		RequestEntity<FileCreationDto> secEntity = new RequestEntity<>(createFileId,
				restTemplateService.headers(request), HttpMethod.POST, restTemplateService.uri(url1));
		MessageResponse response = restTemplate.exchange(secEntity, MessageResponse.class).getBody();

		return response;
	}

	private FileCreationDto insertInfo(byte[] arr, String walletId, UserSecurity secutity) {
		FileCreationDto createFileId = new FileCreationDto();
		createFileId.setFileContent(arr);
		createFileId.setOperatorId(walletId);
		createFileId.setOperatorKey(secutity.getUserPrivateKey());
		return createFileId;
	}

	private String getWalletId(UserSecurity secutity) {
		String walletId = secutity.getAccountShard() + "." + secutity.getAccountRealm() + "."
				+ secutity.getAccountNum();
		return walletId;
	}

	private String convertJsonToFile(String json) throws IOException {

		try {

			// Constructs a FileWriter given a file name, using the platform's default
			// charset
			file = new FileWriter(env.getProperty("create.file"));
			file.write(json.toString());

		} catch (IOException e) {
			e.printStackTrace();

		} finally {

			try {
				file.flush();
				file.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return file.toString();
	}

	@Override
	public ResponseEntity<?> propetyPassportSave(PropertyAddressRequest propertyAddressRequest) {
		User user = userRepository.getById(propertyAddressRequest.getUserId());
		if (user != null) {

			PropertyAddress address = PropertyAddress.builder().postalCode(propertyAddressRequest.getPostalCode())
					.building(propertyAddressRequest.getBuilding()).town(propertyAddressRequest.getTown())
					.country(propertyAddressRequest.getCountry()).userId(user).build();
			address = propertyAddressRepository.save(address);
			return ResponseEntity.ok(
					new MessageResponse(HttpStatus.OK.value(), env.getProperty("property.address.success"), address));

		}
		return ResponseEntity
				.ok(new MessageResponse(HttpStatus.OK.value(), env.getProperty("property.address.failed"), ""));

	}

}