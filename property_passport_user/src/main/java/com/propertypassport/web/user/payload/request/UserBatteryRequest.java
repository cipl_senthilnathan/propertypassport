package com.propertypassport.web.user.payload.request;

import lombok.Data;

@Data
public class UserBatteryRequest {

	private String id;
	private String operatorId;
	
}
