package com.propertypassport.web.user.payload.request;

import lombok.Data;

@Data
public class CreateWalletRequest {

	private String userId;
	private int initialToken;
	
}
