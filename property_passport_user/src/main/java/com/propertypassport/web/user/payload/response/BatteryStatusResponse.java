package com.propertypassport.web.user.payload.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BatteryStatusResponse {

	private double batteryPercent;
	private double batteryCurrentLevel;
	private double batteryToFull;
	private double walletBalance;
	private double estimatedCost;
	private double salePrice;
	private double gridPrice;
	private String walletId;
	private double chargePerMins;
	
}
