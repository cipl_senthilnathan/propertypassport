package com.propertypassport.web.user.payload.request;

import org.joda.time.DateTime;

import lombok.Data;

@Data
public class KycDetailsRequest {

	private String userId;

	private String documentType;

	private String filePath;

	private String documentNumber;

	private String status;
	
	private  DateTime createdTime;

	private String floorPlan;

	private String floorPlanPath;

	private String energyReport;
	
	private String energyReportPath;

	private String valuationReport;
	
	private String valuationReportPatah;

	private String typeOfProperty;

	private int bedrooms;

	private int bathrooms;
	
	private int surfaceArea;

	private String tokenId;

	

}
