package com.propertypassport.web.auth.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.propertypassport.web.auth.payload.request.LoginRequest;
import com.propertypassport.web.auth.payload.request.SignupRequest;
import com.propertypassport.web.auth.service.AuthService;
import com.propertypassport.web.dataaccess.common.request.TokenRefreshRequest;
import com.propertypassport.web.dataaccess.exception.EvBatteryStatusException;
import com.propertypassport.web.dataaccess.exception.PublishingMirrorNodeException;
import com.propertypassport.web.dataaccess.exception.UserInfoException;

@RestController
@CrossOrigin
@RequestMapping("/api/auth")
public class AuthController {

	@Autowired
	AuthService authService;

	@CrossOrigin
	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		return authService.authenticationService(loginRequest);
	}

	@CrossOrigin
	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest, HttpServletRequest request)
			throws PublishingMirrorNodeException, UserInfoException, EvBatteryStatusException   {
		return authService.singUpService(signUpRequest, request);
	}

	@CrossOrigin
	@GetMapping("/find/username")
	public ResponseEntity<?> findUser( @RequestParam(value = "userName") String userName ) {
		
		return authService.findByName( userName );
	}
	
	@CrossOrigin
	@GetMapping("/find/email")
	public ResponseEntity<?> findEmail( @RequestParam(value = "email") String email ) {
		
		return authService.findByEmail( email );
	}
	
	@PostMapping("/refreshtoken/{refreshToken}")
	public ResponseEntity<?> refreshtoken(@Valid @PathVariable(value = "refreshToken")String refreshToken) {
		
		return authService.refreshToken(refreshToken);
	}
	@PostMapping("/otp/verification")
	public ResponseEntity<?> mobileOtpConfirmation(@RequestParam(value="OTP") Long otp,@RequestParam (value="userId") String userId){
		return authService.mobileOtpConfirmation(otp,userId);
	}
}
