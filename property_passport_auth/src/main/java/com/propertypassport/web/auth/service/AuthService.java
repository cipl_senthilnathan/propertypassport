package com.propertypassport.web.auth.service;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.propertypassport.web.auth.payload.request.LoginRequest;
import com.propertypassport.web.auth.payload.request.SignupRequest;
import com.propertypassport.web.dataaccess.exception.EvBatteryStatusException;
import com.propertypassport.web.dataaccess.exception.PublishingMirrorNodeException;
import com.propertypassport.web.dataaccess.exception.UserInfoException;

public interface AuthService {

	ResponseEntity<?> authenticationService(LoginRequest loginRequest);
	ResponseEntity<?> singUpService(SignupRequest signUpRequest, HttpServletRequest request)
			throws PublishingMirrorNodeException, UserInfoException, EvBatteryStatusException;
	
	ResponseEntity<?> findByName(String userName);

	ResponseEntity<?> findByEmail(String email);

	ResponseEntity<?> refreshToken(@Valid String accessToken);

	ResponseEntity<?> mobileOtpConfirmation(Long otp, String userId);
}
