package com.propertypassport.web.auth.service.impl;

import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.propertypassport.web.auth.payload.request.LoginRequest;
import com.propertypassport.web.auth.payload.request.SignupRequest;
import com.propertypassport.web.auth.payload.response.JwtResponse;
import com.propertypassport.web.auth.payload.response.TokenRefreshResponse;
import com.propertypassport.web.auth.service.AuthService;
import com.propertypassport.web.auth.service.EmailService;
import com.propertypassport.web.dataaccess.exception.RoleNotFoundException;
import com.propertypassport.web.dataaccess.exception.TokenRefreshException;
import com.propertypassport.web.dataaccess.exception.UserNotFoundException;
import com.propertypassport.web.dataaccess.exception.UsernameAlreadyExistsException;
import com.propertypassport.web.dataaccess.model.ERole;
import com.propertypassport.web.dataaccess.model.RefreshToken;
import com.propertypassport.web.dataaccess.model.Role;
import com.propertypassport.web.dataaccess.model.User;
import com.propertypassport.web.dataaccess.model.UserInfo;
import com.propertypassport.web.dataaccess.mongo.model.ServiceType;
import com.propertypassport.web.dataaccess.payload.response.MessageResponse;
import com.propertypassport.web.dataaccess.repository.RoleRepository;
import com.propertypassport.web.dataaccess.repository.UserInfoRepository;
import com.propertypassport.web.dataaccess.repository.UserRepository;
import com.propertypassport.web.dataaccess.security.jwt.JwtUtils;
import com.propertypassport.web.dataaccess.services.MongoDBLoggerService;
import com.propertypassport.web.dataaccess.services.RestTemplateService;
import com.propertypassport.web.dataaccess.services.UserDetailsImpl;

@Service
@Transactional
public class AuthServiceImpl implements AuthService {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	EmailService emailService;
	

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	UserInfoRepository userInfoRepository;

	@Autowired
	RestTemplateService restTemplateService;

	@Autowired
	private Environment env;


	private UserInfo userInfo;

	@Autowired
	private RefreshTokenService refreshTokenService;

	@Autowired
	MongoDBLoggerService mongoDBLoggerService;

	@Override
	public ResponseEntity<?> authenticationService(LoginRequest loginRequest) {
		if (!userRepository.existsByUsername(loginRequest.getUsername())) {
			mongoDBLoggerService.createLogger(env.getProperty("user.not.found"), ServiceType.AUTH);
			throw new UserNotFoundException();
		}
			

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		if (Objects.isNull(userDetails)) {
			mongoDBLoggerService.createLogger(env.getProperty("user.not.found"), ServiceType.AUTH);
			throw new UserNotFoundException();
		}
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());
		// creating refresh token
		RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getId());

		return ResponseEntity.ok(new MessageResponse(HttpStatus.OK.value(), env.getProperty("user.login.sucess"),
				new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), roles,
						refreshToken.getToken())));
	}

	@Override
	
	public ResponseEntity<?> singUpService(SignupRequest signUpRequest, HttpServletRequest request) 
			 {

		// Create new user's account
		User user = User.builder().mobileNum(signUpRequest.getMobileNumber()).email(signUpRequest.getEmail())
				.password(encoder.encode(signUpRequest.getPassword()))
				.username(String.valueOf(signUpRequest.getMobileNumber())).build();

			if (userRepository.existsByUsername(String.valueOf(signUpRequest.getMobileNumber()))) {
				throw new UsernameAlreadyExistsException();
			}

			if (userRepository.existsByEmail(signUpRequest.getEmail())) {
				return ResponseEntity.badRequest()
						.body(new MessageResponse(env.getProperty("email.exist"), HttpStatus.BAD_REQUEST.value()));
			}

			Set<String> strRoles = signUpRequest.getRole();
			Set<Role> roles = new HashSet<>();

			user.setRoles(getRole(strRoles, roles));

			user = userRepository.save(user);

			userInfo = insertUserInfo(signUpRequest, user);
			
			return ResponseEntity.ok(
					new MessageResponse(HttpStatus.OK.value(), env.getProperty("user.registered.success"), user));
			

	}

	private Set<Role> getRole(Set<String> strRoles, Set<Role> roles) {
		if (Objects.isNull(strRoles)) {
			if (!roleRepository.existsByName(ERole.ROLE_USER)) {
				throw new RoleNotFoundException();
			}
			Role userRole = roleRepository.findByName(ERole.ROLE_USER).get();
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "ADMIN":
					if (!roleRepository.existsByName(ERole.ROLE_ADMIN)) {
						throw new RoleNotFoundException();
					}
					Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN).get();
					roles.add(adminRole);
					break;
				case "MANAGER":
					if (!roleRepository.existsByName(ERole.ROLE_MANAGER)) {
						throw new RoleNotFoundException();
					}
					Role modRole = roleRepository.findByName(ERole.ROLE_MANAGER).get();
					roles.add(modRole);
					break;
				case "USER":
					if (!roleRepository.existsByName(ERole.ROLE_USER)) {
						throw new RoleNotFoundException();
					}
					Role userRole = roleRepository.findByName(ERole.ROLE_USER).get();
					roles.add(userRole);
					break;
				default:
					if (!roleRepository.existsByName(ERole.ROLE_USER)) {
						throw new RoleNotFoundException();
					}
					userRole = roleRepository.findByName(ERole.ROLE_USER).get();
					roles.add(userRole);
				}
			});
		}

		return roles;
	}

	
	private UserInfo insertUserInfo(SignupRequest signUpRequest, User user)  {

		String otp = new DecimalFormat("000000").format(new Random().nextInt(999999));

		String mobno = "+91" + String.valueOf(signUpRequest.getMobileNumber());
//
//		Twilio.init(PowertransitionConstant.ACCOUNT_SID, PowertransitionConstant.AUTH_TOKEN);
//		Message message = Message.creator(new com.twilio.type.PhoneNumber(mobno),
//				new com.twilio.type.PhoneNumber(PowertransitionConstant.phoneNumber), otp).create();
//		System.out.println(message);
		userInfo = UserInfo.builder().userId(user).firstName(signUpRequest.getFirstName())
				.lastName(signUpRequest.getLastName()).emailAddress(signUpRequest.getEmail())
				.mobileNumber(signUpRequest.getMobileNumber()).validateOtp(Long.parseLong(otp)).build();

		userInfo = userInfoRepository.save(userInfo);
		emailService.sendEmailDEV(user.getEmail(), "Register Verification For Property Passport",
				"Thanks For Registration your OTP :" + otp);
		return userInfo;
	}

	@Override
	public ResponseEntity<?> findByName(String userName) {
		try {
			User user = userRepository.findByUsername(userName).get();
			if (user != null) {
				return ResponseEntity
						.ok(new MessageResponse(HttpStatus.OK.value(), env.getProperty("user.exist"), user));
			}
			return ResponseEntity
					.ok(new MessageResponse(HttpStatus.BAD_REQUEST.value(), env.getProperty("user.not.found"), user));

		} catch (Exception e) {
			mongoDBLoggerService.createLogger(env.getProperty("user.not.found"), ServiceType.AUTH);
			return ResponseEntity
					.ok(new MessageResponse(env.getProperty("user.not.found"), HttpStatus.BAD_REQUEST.value()));
		}
	}

	@Override
	public ResponseEntity<?> findByEmail(String email) {
		try {
			Boolean flag = userRepository.existsByEmail(email);
			if (flag) {
				return ResponseEntity
						.ok(new MessageResponse(HttpStatus.OK.value(), env.getProperty("email.exist"), flag));
			}
			return ResponseEntity
					.ok(new MessageResponse(HttpStatus.BAD_REQUEST.value(), env.getProperty("email.not.found"), flag));

		} catch (Exception e) {
			mongoDBLoggerService.createLogger(env.getProperty("email.not.found"), ServiceType.AUTH);
			return ResponseEntity
					.ok(new MessageResponse(env.getProperty("email.not.found"), HttpStatus.BAD_REQUEST.value()));
		}
	}

	@Override
	public ResponseEntity<?> refreshToken(@Valid String accessToken) {
		// TODO Auto-generated method stub

		return refreshTokenService.findByToken(accessToken)
				// .map(refreshTokenService::verifyExpiration)
				.map(RefreshToken::getUser).map(user -> {
					String token = jwtUtils.generateTokenFromUsername(user.getUsername());
					return ResponseEntity.ok(new TokenRefreshResponse(token, accessToken, HttpStatus.OK.value()));
				}).orElseThrow(() -> new TokenRefreshException(accessToken, env.getProperty("refresh.token.not.in.db"),
						HttpStatus.UNAUTHORIZED.value()));

	}
	@Override
	public ResponseEntity<?> mobileOtpConfirmation(Long otp, String userId) {
		UserInfo userInfo = userInfoRepository.getById(userId);
		if (userInfo.getValidateOtp() == otp) {
			return ResponseEntity.ok(
					new MessageResponse(HttpStatus.OK.value(), env.getProperty("otp.validation.success"), userInfo));
		}
		return ResponseEntity
				.ok(new MessageResponse(HttpStatus.BAD_REQUEST.value(), env.getProperty("enter.correct.otp"), ""));

	}

}
