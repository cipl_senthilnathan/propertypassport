package com.propertypassport.web.auth.payload.request;

import java.util.Set;

import javax.validation.constraints.*;

import lombok.Data;

@Data
public class SignupRequest {
 
    @NotBlank
    @Email
    private String email;
    
    @NotNull
    private Set<String> role;
    
    @NotBlank
    private String password;
  
    
    private String firstName;
	
	private String lastName;
	
	
	private long mobileNumber;
}
