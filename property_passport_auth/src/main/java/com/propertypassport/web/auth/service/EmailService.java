package com.propertypassport.web.auth.service;

import org.springframework.stereotype.Service;

@Service
public interface EmailService {

	boolean sendEmail(String username, String subject, String content);

	boolean sendEmailDEV(String toEmailId, String subject, String content);

}
