package com.propertypassport.web.hts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.propertypassport.web.dataaccess.payload.request.NFTTokenRequest;
import com.propertypassport.web.dataaccess.payload.response.MessageResponse;
import com.propertypassport.web.hts.payload.request.TokenBean;
import com.propertypassport.web.hts.service.HederaTokenService;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/token")
public class HederaTokenServiceController {

	@Autowired
	private HederaTokenService pTTokenService;

	/*
	 * It's handle only restApi request should starts with /api/token/balance and
	 * once request handled, it will pass to another service level file for further
	 * business process once the business process done this will take care of
	 * response to sender
	 */
	@PreAuthorize("hasRole('USER') or hasRole('MANAGER') or hasRole('ADMIN')")
	@RequestMapping(value = "/balance", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<MessageResponse> balanceToken( @RequestBody String accountId ) throws Exception {
		return pTTokenService.balanceToken( accountId );
	}
	
	/*
	 * It's handle only restApi request should starts with /api/token/transfer
	 * and once request handled, it will pass to another service level file for further business process
	 * once the business process done this will take care of response to sender
	 */
	@RequestMapping(value = "/transfer", method = RequestMethod.POST)
	public ResponseEntity<?> transferToken(@RequestBody TokenBean tokenBean) throws Exception {
		return pTTokenService.transferToken(tokenBean);
	}
	
	@PreAuthorize("hasRole('USER') or hasRole('MANAGER') or hasRole('ADMIN')")
	@RequestMapping(value = "/associatkycuser", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<MessageResponse> associatKycUser(@RequestBody TokenBean tokenBean) throws Exception {
		return pTTokenService.associatKycUser(tokenBean);
	}
	
	@PreAuthorize("hasRole('USER')")
	@RequestMapping(value = "/createNFTToken", method = RequestMethod.POST)
	public ResponseEntity<MessageResponse> createNFTToken(@RequestBody NFTTokenRequest nftTokenRequest){
		
		return pTTokenService.createNFTToken(nftTokenRequest);
	}
}
