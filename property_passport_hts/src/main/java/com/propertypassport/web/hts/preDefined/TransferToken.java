package com.propertypassport.web.hts.preDefined;

import java.util.Objects;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hedera.hashgraph.sdk.AccountId;
import com.hedera.hashgraph.sdk.Client;
import com.hedera.hashgraph.sdk.HederaPreCheckStatusException;
import com.hedera.hashgraph.sdk.HederaReceiptStatusException;
import com.hedera.hashgraph.sdk.PrivateKey;
import com.hedera.hashgraph.sdk.TokenId;
import com.hedera.hashgraph.sdk.TransactionResponse;
import com.hedera.hashgraph.sdk.TransferTransaction;

/*
 * THIS IS EXAMPLE TEST CLASS USED TO TransferToken
 */
public class TransferToken {
	private static final Logger logger = LoggerFactory.getLogger(TransferToken.class);

	private static final AccountId ADMIN_OPERATOR_ID = AccountId.fromString(Objects.requireNonNull("0.0.4389"));
	private static final AccountId OPERATOR_ID_FROM = AccountId.fromString(Objects.requireNonNull("0.0.55398"));
	private static final PrivateKey ADMIN_OPERATOR_KEY = PrivateKey.fromString(Objects.requireNonNull(
			"302e020100300506032b657004220420b56be9ea5c14be403fa44280fe85457ccbda9388e161b10ce1bfa072d1fcd2ab"));

	public static void main(String[] args)
			throws TimeoutException, HederaPreCheckStatusException, HederaReceiptStatusException {

		Client client = Client.forTestnet();
		client.setOperator(ADMIN_OPERATOR_ID, ADMIN_OPERATOR_KEY);
		// Create the transfer transaction
		TransferTransaction transaction = new TransferTransaction().setTransactionMemo("testing token")
				.addTokenTransfer(TokenId.fromString("0.0.209992"), ADMIN_OPERATOR_ID, -400)
				.addTokenTransfer(TokenId.fromString("0.0.209992"), OPERATOR_ID_FROM, 400).freezeWith(client);

		// Sign with the client operator key and submit the transaction to a Hedera
		
		TransactionResponse txResponse = transaction.sign(ADMIN_OPERATOR_KEY).execute(client);

		logger.info("The transaction consensus status is " + txResponse.getRecord(client));

	}
}
