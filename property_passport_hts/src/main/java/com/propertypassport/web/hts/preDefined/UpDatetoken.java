package com.propertypassport.web.hts.preDefined;

import java.util.Objects;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hedera.hashgraph.sdk.AccountId;
import com.hedera.hashgraph.sdk.Client;
import com.hedera.hashgraph.sdk.HederaPreCheckStatusException;
import com.hedera.hashgraph.sdk.HederaReceiptStatusException;
import com.hedera.hashgraph.sdk.PrivateKey;
import com.hedera.hashgraph.sdk.Status;
import com.hedera.hashgraph.sdk.TokenId;
import com.hedera.hashgraph.sdk.TokenUpdateTransaction;
import com.hedera.hashgraph.sdk.TransactionReceipt;
import com.hedera.hashgraph.sdk.TransactionResponse;
	/*
	 * THIS IS EXAMPLE TEST CLASS USED TO UpDatetoken
	 */
public class UpDatetoken {
	private static final Logger logger = LoggerFactory.getLogger(UpDatetoken.class);
	
	private static final AccountId OPERATOR_ID = AccountId.fromString(Objects.requireNonNull("0.0.4389"));
    private static final PrivateKey OPERATOR_KEY = PrivateKey.fromString(Objects.requireNonNull("302e020100300506032b657004220420b56be9ea5c14be403fa44280fe85457ccbda9388e161b10ce1bfa072d1fcd2ab"));
	public static void main(String[] args) throws TimeoutException, HederaPreCheckStatusException, HederaReceiptStatusException {
		
		Client client = Client.forTestnet();
        client.setOperator(OPERATOR_ID, OPERATOR_KEY);
      //Create the transaction 
        TokenUpdateTransaction transaction = new TokenUpdateTransaction()
             .setTokenId(new TokenId(0,0,178064))
             .setTokenName("UpDate Token");

        //Freeze the unsigned transaction, sign with the admin private key of the token, submit the transaction to a Hedera network
        TransactionResponse txResponse = transaction.freezeWith(client).sign(OPERATOR_KEY).execute(client);

        //Request the receipt of the transaction
        TransactionReceipt receipt = txResponse.getReceipt(client);

        //Get the transaction consensus status
        Status transactionStatus = receipt.status;

        logger.info("The transaction consensus status is " +transactionStatus);
	}
}
