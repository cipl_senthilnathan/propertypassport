package com.propertypassport.web.hts.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.hedera.hashgraph.sdk.AccountBalanceQuery;
import com.hedera.hashgraph.sdk.AccountId;
import com.hedera.hashgraph.sdk.Client;
import com.hedera.hashgraph.sdk.Hbar;
import com.hedera.hashgraph.sdk.HederaPreCheckStatusException;
import com.hedera.hashgraph.sdk.Status;
import com.hedera.hashgraph.sdk.TokenAssociateTransaction;
import com.hedera.hashgraph.sdk.TokenGrantKycTransaction;
import com.hedera.hashgraph.sdk.TokenId;
import com.hedera.hashgraph.sdk.TransactionResponse;
import com.hedera.hashgraph.sdk.Transfer;
import com.hedera.hashgraph.sdk.TransferTransaction;
import com.propertypassport.web.dataaccess.exception.BalanceTokenNotFoundException;
import com.propertypassport.web.dataaccess.mongo.model.ServiceType;
import com.propertypassport.web.dataaccess.payload.request.NFTTokenRequest;
import com.propertypassport.web.dataaccess.payload.response.MessageResponse;
import com.propertypassport.web.dataaccess.payload.response.TokenTransferResponse;
import com.propertypassport.web.dataaccess.services.MongoDBLoggerService;
import com.propertypassport.web.hts.payload.request.TokenBean;
import com.propertypassport.web.hts.service.HederaTokenService;
import com.propertypassport.web.hts.service.TokenHelperService;
import com.propertypassport.web.hts.utils.NFTTokenUtil;

@Service(value = "HederaTokenServiceImpl")
public class HederaTokenServiceImpl implements HederaTokenService {

	@Autowired
	TokenHelperService tokenHelper;

	Client client = null;

	@Value("${mobile.app.tokenid}")
	private String tokenId;

	private Map tokenBalance = new HashMap();

	private Status transactionStatus = null;

	@Autowired
	private Environment env;
	@Autowired
	MongoDBLoggerService mongoDBLoggerService;

	@Override
	public ResponseEntity<MessageResponse> balanceToken(String accountId) {
		client = tokenHelper.getAdminClient();

		try {
			Hbar balance = new AccountBalanceQuery().setAccountId(tokenHelper.getCustomOperatorId(accountId))
					.execute(client).hbars;
			if (Objects.isNull(tokenBalance) || tokenBalance.isEmpty()) {
				return ResponseEntity.ok(new MessageResponse(HttpStatus.PARTIAL_CONTENT.value(),
						env.getProperty("token.balance.failed"), null));
			}
			return ResponseEntity.ok(new MessageResponse(HttpStatus.OK.value(),
					env.getProperty("token.balance.success"), balance.toString()));
		} catch (Exception e) {
			mongoDBLoggerService.createLogger(e.getMessage(), ServiceType.HTS);
			return ResponseEntity
					.ok(new MessageResponse(HttpStatus.PARTIAL_CONTENT.value(), e.getMessage(), accountId));
		}
	}

	@Override
	public ResponseEntity<?> transferToken(TokenBean tokenBean) {
		client = tokenHelper.getTreasuryClient();
		TransactionResponse txResponse = null;
		Hbar amount = Hbar.fromTinybars(tokenBean.getAmount());
		try {
			txResponse = new TransferTransaction()
					.addHbarTransfer(AccountId.fromString(tokenBean.getOperatorId()), amount.negated())
					.addHbarTransfer(AccountId.fromString(tokenBean.getToAccountId()), amount)
					.setTransactionMemo("transfer amount").execute(client);

			List<Transfer> transactionChargeList = txResponse.getRecord(client).transfers;

			tokenBean.setOperatorId(tokenBean.getFromSenderId());
			tokenBean.setOperatorKey(tokenBean.getFromSenderKey());
			String remainingBalance = "";
			try {
				Hbar balance = getRemainingBalance(AccountId.fromString(tokenBean.getOperatorId()));
				remainingBalance = balance.toString();
			} catch (BalanceTokenNotFoundException e) {
				// TODO: handle exception
				mongoDBLoggerService.createLogger(env.getProperty("balance.token.not.found"), ServiceType.HTS);
				return ResponseEntity.ok(env.getProperty("balance.token.not.found"));
			}

			TokenTransferResponse tokenTransferResponse = TokenTransferResponse.builder()
					.netWorkFee(transactionChargeList.get(1).amount.getValue())
					.nodeFee(transactionChargeList.get(0).amount.getValue())
					.transactionFee(transactionChargeList.get(2).amount.getValue())
					.balanceRemain(Double.parseDouble(remainingBalance)).build();

			return ResponseEntity.ok(new MessageResponse(HttpStatus.OK.value(),
					env.getProperty("token.transfer.success"), tokenTransferResponse));
		} catch (Exception e) {
			mongoDBLoggerService.createLogger(e.getMessage(), ServiceType.HTS);
			return ResponseEntity
					.ok(new MessageResponse(HttpStatus.PARTIAL_CONTENT.value(), e.getMessage(), tokenBalance));
		}
	}

	@Override
	public ResponseEntity<MessageResponse> associatKycUser(TokenBean tokenBean) {

		try {
			associateToken(tokenBean);
			enableKYC(tokenBean);
		} catch (Exception e) {
			// TODO: handle exception
			mongoDBLoggerService.createLogger(e.getMessage(), ServiceType.HTS);
			return ResponseEntity.ok(MessageResponse.builder().status(HttpStatus.PARTIAL_CONTENT.value())
					.message(e.getMessage()).build());
		}
		tokenBean.setFromSenderId(tokenHelper.getTreasuryOperatorIdString());
		tokenBean.setFromSenderKey(tokenHelper.getTreasuryOperatorKeyString());
		tokenBean.setToAccountId(tokenBean.getOperatorId());
		tokenBean.setToAccountKey(tokenBean.getOperatorKey());
		String initailBalanceToken = "";
		try {
			initailBalanceToken = initialBalanceToken(tokenBean);
		} catch (BalanceTokenNotFoundException e) {
			// TODO: handle exception
			mongoDBLoggerService.createLogger(e.getMessage(), ServiceType.HTS);
			return ResponseEntity.ok(MessageResponse.builder().status(HttpStatus.PARTIAL_CONTENT.value())
					.message(e.getMessage()).build());
		}

		return ResponseEntity.ok(MessageResponse.builder().status(HttpStatus.OK.value())
				.message(env.getProperty("associate.kyc.user.success")).response(initailBalanceToken).build());
	}

	private String initialBalanceToken(TokenBean tokenBean) throws BalanceTokenNotFoundException {
		client = tokenHelper.getAdminClient();

		try {
			long balanceLong = 0;
			tokenBalance = new AccountBalanceQuery()
					.setAccountId(tokenHelper.getCustomOperatorId(tokenBean.getOperatorId())).execute(client).token;

			if (Objects.nonNull(tokenBalance) && !tokenBalance.isEmpty()) {
				balanceLong = (long) tokenBalance.get(AccountId.fromString(tokenBean.getOperatorId()));
			}
			return Long.toString(balanceLong);
		} catch (Exception e) {
			mongoDBLoggerService.createLogger(env.getProperty("balance.token.not.found"), ServiceType.HTS);
			throw new BalanceTokenNotFoundException();
		}
	}

	private void enableKYC(TokenBean tokenBean) throws Exception {
		client = tokenHelper.getAdminClient();

		try {

			transactionStatus = new TokenGrantKycTransaction()
					.setAccountId(tokenHelper.getCustomOperatorId(tokenBean.getOperatorId()))
					.setTokenId(TokenId.fromString(tokenBean.getTokenId())).freezeWith(client)
					.sign(tokenHelper.getTreasuryOperatorKey()).execute(client).getReceipt(client).status;
		} catch (Exception e) {
			mongoDBLoggerService.createLogger(e.getMessage(), ServiceType.HTS);
			throw e;
		}
	}

	private void associateToken(TokenBean tokenBean) throws Exception {
		client = tokenHelper.getAdminClient();

		try {
			System.out.println("tokenBean.getOperatorId()" + tokenBean.getOperatorId());
			transactionStatus = new TokenAssociateTransaction()
					.setAccountId(tokenHelper.getCustomOperatorId(tokenBean.getOperatorId()))
					.setMaxTransactionFee(new Hbar(100)).setTokenIds(TokenId.fromString(tokenBean.getTokenId()))
					.freezeWith(client).sign(tokenHelper.getCustomOperatorKey(tokenBean.getOperatorKey()))
					.execute(client).getReceipt(client).status;
		} catch (Exception e) {
			mongoDBLoggerService.createLogger(e.getMessage(), ServiceType.HTS);
			throw e;
		}
	}

	private Hbar getRemainingBalance(AccountId accountId) throws TimeoutException, HederaPreCheckStatusException {
		Hbar remainingBalance = new AccountBalanceQuery().setAccountId(accountId).execute(client).hbars;
		return remainingBalance;
	}

	@Override
	public ResponseEntity<MessageResponse> createNFTToken(NFTTokenRequest nftTokenRequest) {
		// TODO Auto-generated method stub
		 String tokenId = null;
		NFTTokenUtil nftTokenUtil = new NFTTokenUtil();
		

		try {
			TokenId nftTokenId = nftTokenUtil.createToken(nftTokenRequest.getFileId(), nftTokenRequest.getOperatorId(),
					nftTokenRequest.getOperatorKey());
			if (Objects.nonNull(nftTokenId)) {
				tokenId=nftTokenId.toString();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.ok(MessageResponse.builder().status(HttpStatus.OK.value())
				.message("NFT token created successfully").response(tokenId).build());
	}
}
