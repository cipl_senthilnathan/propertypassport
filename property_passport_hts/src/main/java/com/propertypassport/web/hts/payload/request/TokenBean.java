package com.propertypassport.web.hts.payload.request;

import lombok.Builder;
import lombok.Data;

/*
	 * TokenBean class used to receive inputs and pass to service 
	 * receive inputs values with the help of getters and setters
	 */
@Data
@Builder
public class TokenBean {

	private long amount;
	private String operatorId;
	private String operatorKey;
	private String tokenId;
	private String fromSenderId;
	private String fromSenderKey;
	private String toAccountId;
	private String toAccountKey;
	
}

