package com.propertypassport.web.hts.preDefined;

import java.util.Objects;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hedera.hashgraph.sdk.AccountId;
import com.hedera.hashgraph.sdk.Client;
import com.hedera.hashgraph.sdk.HederaPreCheckStatusException;
import com.hedera.hashgraph.sdk.HederaReceiptStatusException;
import com.hedera.hashgraph.sdk.PrivateKey;
import com.hedera.hashgraph.sdk.Status;
import com.hedera.hashgraph.sdk.TokenGrantKycTransaction;
import com.hedera.hashgraph.sdk.TokenId;
import com.hedera.hashgraph.sdk.TransactionReceipt;
import com.hedera.hashgraph.sdk.TransactionResponse;
	/*
	 * THIS IS EXAMPLE TEST CLASS USED TO EnableKYCAccountFlag
	 */
public class EnableKYCAccountFlag {
	private static final Logger logger = LoggerFactory.getLogger(EnableKYCAccountFlag.class);
	
	private static final AccountId ADMIN_OPERATOR_ID = AccountId.fromString(Objects.requireNonNull("0.0.4389"));
	private static final AccountId OPERATOR_ID_TO = AccountId.fromString(Objects.requireNonNull("0.0.215097"));
    private static final PrivateKey ADMIN_OPERATOR_KEY = PrivateKey.fromString(Objects.requireNonNull("302e020100300506032b657004220420b56be9ea5c14be403fa44280fe85457ccbda9388e161b10ce1bfa072d1fcd2ab"));
    private static final PrivateKey OPERATOR_KEY_FROM = PrivateKey.fromString(Objects.requireNonNull("302e020100300506032b65700422042004ccc8ee33fb3e16a8d174e16d1ca3bb65220b874b6b3164f5de1dbc6a6ca7e3"));
   
    public static void main(String[] args) throws TimeoutException, HederaPreCheckStatusException, HederaReceiptStatusException {
		
		Client client = Client.forTestnet();
        client.setOperator(ADMIN_OPERATOR_ID, ADMIN_OPERATOR_KEY);
        //Enable KYC flag on account
        TokenGrantKycTransaction transaction = new TokenGrantKycTransaction()
            .setAccountId(OPERATOR_ID_TO)
            .setTokenId(TokenId.fromString("0.0.209992"));

        //Freeze the unsigned transaction, sign with the kyc private key of the token, submit the transaction to a Hedera network
        TransactionResponse txResponse = transaction.freezeWith(client).sign(OPERATOR_KEY_FROM).execute(client);
            
        //Request the receipt of the transaction
        TransactionReceipt receipt = txResponse.getReceipt(client);
            
        //Obtain the transaction consensus status
        Status transactionStatus = receipt.status;

        logger.info("The transaction consensus status is " +transactionStatus);

   
	}
}
