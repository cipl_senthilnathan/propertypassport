package com.propertypassport.web.hts.preDefined;

import java.util.Objects;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hedera.hashgraph.sdk.AccountBalance;
import com.hedera.hashgraph.sdk.AccountBalanceQuery;
import com.hedera.hashgraph.sdk.AccountId;
import com.hedera.hashgraph.sdk.Client;
import com.hedera.hashgraph.sdk.HederaPreCheckStatusException;
import com.hedera.hashgraph.sdk.HederaReceiptStatusException;
import com.hedera.hashgraph.sdk.PrivateKey;
import com.hedera.hashgraph.sdk.TokenId;

/*
 * THIS IS EXAMPLE TEST CLASS USED TO GetBalanceToken
 */
public class GetBalanceToken {
	private static final Logger logger = LoggerFactory.getLogger(GetBalanceToken.class);

	private static final AccountId OPERATOR_ID_FROM = AccountId.fromString(Objects.requireNonNull("0.0.55398"));
	private static final AccountId OPERATOR_ID_TO = AccountId.fromString(Objects.requireNonNull("0.0.55806"));
	
	private static final PrivateKey OPERATOR_KEY_FROM = PrivateKey.fromString(Objects.requireNonNull(
			"302e020100300506032b65700422042004ccc8ee33fb3e16a8d174e16d1ca3bb65220b874b6b3164f5de1dbc6a6ca7e3"));
	

	public static void main(String[] args)
			throws TimeoutException, HederaPreCheckStatusException, HederaReceiptStatusException {

		Client client = Client.forTestnet();
		client.setOperator(OPERATOR_ID_FROM, OPERATOR_KEY_FROM);
		AccountBalanceQuery query = new AccountBalanceQuery().setAccountId(OPERATOR_ID_FROM);

		// Sign with the operator private key and submit to a Hedera network
		AccountBalance tokenBalance = query.execute(client);

		AccountBalanceQuery query1 = new AccountBalanceQuery().setAccountId(OPERATOR_ID_TO);

		// Sign with the operator private key and submit to a Hedera network
		AccountBalance tokenBalance1 = query1.execute(client);

		logger.info("The token balance(s) for this account: " + tokenBalance.token);
		logger.info(
				"The token balance(s) for this account: " + tokenBalance.token.get(TokenId.fromString("0.0.282659")));
	}
}
