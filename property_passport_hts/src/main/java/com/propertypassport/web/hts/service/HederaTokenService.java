package com.propertypassport.web.hts.service;

import org.springframework.http.ResponseEntity;

import com.propertypassport.web.dataaccess.payload.request.NFTTokenRequest;
import com.propertypassport.web.dataaccess.payload.response.MessageResponse;
import com.propertypassport.web.hts.payload.request.TokenBean;


/*
	 * PTTokenService is a boiler plates in RestAPI which is used to communicating and working together with business logics
	 */
public interface HederaTokenService {

	public ResponseEntity<MessageResponse> balanceToken( String accountId );

	public ResponseEntity<?> transferToken(TokenBean tokenBean);

	public ResponseEntity<MessageResponse> associatKycUser(TokenBean tokenBean);
	
	public ResponseEntity<MessageResponse> createNFTToken(NFTTokenRequest nftTokenRequest);
	
}
