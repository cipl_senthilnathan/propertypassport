package com.propertypassport.web.hts.preDefined;

import java.util.Objects;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hedera.hashgraph.sdk.AccountId;
import com.hedera.hashgraph.sdk.Client;
import com.hedera.hashgraph.sdk.Hbar;
import com.hedera.hashgraph.sdk.HederaPreCheckStatusException;
import com.hedera.hashgraph.sdk.HederaReceiptStatusException;
import com.hedera.hashgraph.sdk.PrivateKey;
import com.hedera.hashgraph.sdk.Status;
import com.hedera.hashgraph.sdk.TokenAssociateTransaction;
import com.hedera.hashgraph.sdk.TokenId;
import com.hedera.hashgraph.sdk.TransactionReceipt;
import com.hedera.hashgraph.sdk.TransactionResponse;

/*
 * THIS IS EXAMPLE TEST CLASS USED TO ASSOCIATETOKEN
 */
public class AssociateToken {
	private static final Logger logger = LoggerFactory.getLogger(AssociateToken.class);

	private static final AccountId ADMIN_OPERATOR_ID = AccountId.fromString(Objects.requireNonNull("0.0.4389"));
	private static final AccountId OPERATOR_ID_FROM = AccountId.fromString(Objects.requireNonNull("0.0.215097"));
	private static final PrivateKey ADMIN_OPERATOR_KEY = PrivateKey.fromString(Objects.requireNonNull(
			"302e020100300506032b657004220420b56be9ea5c14be403fa44280fe85457ccbda9388e161b10ce1bfa072d1fcd2ab"));
	private static final PrivateKey OPERATOR_KEY_FROM = PrivateKey.fromString(Objects.requireNonNull(
			"302e020100300506032b65700422042098d5fc471fd4377f62346c2b624ce66cdd411fecfe09d960e0ea7fa057694680"));
	

	public static void main(String[] args)
			throws TimeoutException, HederaPreCheckStatusException, HederaReceiptStatusException {

		Client client = Client.forTestnet();
		client.setOperator(ADMIN_OPERATOR_ID, ADMIN_OPERATOR_KEY);
		TokenAssociateTransaction transaction = new TokenAssociateTransaction().setAccountId(OPERATOR_ID_FROM)
				.setMaxTransactionFee(new Hbar(100)).setTokenIds(TokenId.fromString("0.0.209992"));

		// Freeze the unsigned transaction, sign with the private key of the account
		// that is being associated to a token, submit the transaction to a Hedera
		// network
		TransactionResponse txResponse = transaction.freezeWith(client).sign(OPERATOR_KEY_FROM).execute(client);

		// Request the receipt of the transaction
		TransactionReceipt receipt = txResponse.getReceipt(client);

		// Get the transaction consensus status
		Status transactionStatus = receipt.status;

		logger.info("The transaction consensus status " + transactionStatus);
	}
}
