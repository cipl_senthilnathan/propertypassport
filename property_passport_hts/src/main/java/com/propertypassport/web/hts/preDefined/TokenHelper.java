package com.propertypassport.web.hts.preDefined;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.hedera.hashgraph.sdk.AccountId;
import com.hedera.hashgraph.sdk.Client;
import com.hedera.hashgraph.sdk.PrivateKey;

import io.github.cdimascio.dotenv.Dotenv;
	/*
	 * THIS IS HELPE CLASS USED TO DEFINE AND PASS .ENV FILE VALUES
	 */
@Service(value = "TokenHelper")
public class TokenHelper {

	public Client client = null;

	public String netWork = "";
	
	private static final Logger logger = LoggerFactory.getLogger(TokenHelper.class);

	public TokenHelper() {
		Properties properties = new Properties();
		InputStream inputStream = TokenHelper.class.getClassLoader().getResourceAsStream("application.properties");
		if (Objects.nonNull(inputStream)) {
			try {
				properties.load(inputStream);
			} catch (IOException e) {
				logger.info(" error in Helper Class : " + e.getMessage());
			}
		}
		if (properties.getProperty("network").equalsIgnoreCase("mainnet")) {
			client = Client.forMainnet();
			netWork = "mainnet";
		} else if (properties.getProperty("network").equalsIgnoreCase("testnet")) {
			client = Client.forTestnet();
			netWork = "testnet";
		}

	}

	private Dotenv getEnv() {
		if (netWork.equalsIgnoreCase("mainnet")) {
			return Dotenv.configure().directory("src/main/credential/mainnet-env").filename(".env").load();
		} else if (netWork.equalsIgnoreCase("testnet")) {
			return Dotenv.configure().directory("src/main/credential/testnet-env").filename(".env").load();
		}
		return null;
	}

	public Client getAdminClient() {
		client.setOperator(getAdminOperatorId(), getAdminOperatorKey());
		return client;
	}

	public AccountId getAdminOperatorId() {
		return AccountId.fromString(Objects.requireNonNull(getEnv().get("ADMIN_OPERATOR_ID")));
	}

	public PrivateKey getAdminOperatorKey() {
		return PrivateKey.fromString(Objects.requireNonNull(getEnv().get("ADMIN_OPERATOR_KEY")));
	}

	public Client getTreasuryClient() {
		client.setOperator(getTreasuryOperatorId(), getTreasuryOperatorKey());
		return client;
	}

	public AccountId getTreasuryOperatorId() {
		return AccountId.fromString(Objects.requireNonNull(getEnv().get("TREASURY_OPERATOR_ID")));
	}

	public PrivateKey getTreasuryOperatorKey() {
		return PrivateKey.fromString(Objects.requireNonNull(getEnv().get("TREASURY_OPERATOR_KEY")));
	}
	
	public AccountId getGridOperatorId() {
		return AccountId.fromString(Objects.requireNonNull(getEnv().get("OPERATOR_ID_OPERATOR")));
	}

	public PrivateKey getGridOperatorKey() {
		return PrivateKey.fromString(Objects.requireNonNull(getEnv().get("OPERATOR_KEY_OPERATOR")));
	}

	public Client getCustomClient(TokenBean token) {
		client.setOperator(getCustomOperatorId(token), getCustomOperatorKey(token));
		return client;
	}

	public AccountId getCustomOperatorId(TokenBean token) {
		return AccountId.fromString(token.getOperatorId());
	}

	public PrivateKey getCustomOperatorKey(TokenBean token) {
		return PrivateKey.fromString(token.getOperatorKey());
	}

	public AccountId getCustomFromOperatorId(TokenBean token) {
		return AccountId.fromString(token.getFromSenderId());
	}

	public PrivateKey getCustomFromOperatorKey(TokenBean token) {
		return PrivateKey.fromString(token.getFromSenderKey());
	}

	public AccountId getCustomToOperatorId(TokenBean token) {
		return AccountId.fromString(token.getToAccountId());
	}

	public PrivateKey getCustomToOperatorKey(TokenBean token) {
		return PrivateKey.fromString(token.getToAccountKey());
	}
}
