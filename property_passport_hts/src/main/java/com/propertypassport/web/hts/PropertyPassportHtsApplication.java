package com.propertypassport.web.hts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication(scanBasePackages = {"com.propertypassport.web","com.propertypassport.web.*"} )
@PropertySources({ @PropertySource("classpath:env.properties"),@PropertySource("classpath:message.properties") })
public class PropertyPassportHtsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PropertyPassportHtsApplication.class, args);
	}

}
